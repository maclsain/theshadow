var thisMinigameObject = argument0;
var thisPlayer = argument1;
var thisMinigameObjectName = object_get_name(thisMinigameObject);
var objectInstance = 0;

gameType = string_char_at(thisMinigameObjectName,5);
show_debug_message(gameType);
show_debug_message(thisMinigameObjectName);
switch(gameType){
	case "E": //exercise
		instance_create_layer(x,y,global.glob_layerMGamePresentation,obj_exerciseMGame);
		break;
	case "C": //cooking
		instance_create_layer(x,y,global.glob_layerMGamePresentation,obj_cookMGame);
		break;
	case "L": //laundry
		instance_create_layer(x,y,global.glob_layerMGamePresentation,obj_laundryMGame);
		break;
	case "W": //work
		instance_create_layer(x,y,global.glob_layerMGamePresentation,obj_workMGame);
		break;
	default:
		show_debug_message("Error in Minigame Type");
		break;
}

return 0;