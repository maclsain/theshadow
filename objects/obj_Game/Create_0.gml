/// @description Insert description here
// You can write your code in this editor

global.glob_gridSize = 64;
global.glob_maxDepth = 400;
global.glob_minDepth = -400;
global.glob_resolutionW = 640;
global.glob_resolutionH = 576;

this_view_camera = view_get_camera(view_current);

window_set_size(global.glob_resolutionW, global.glob_resolutionH);
camera_set_view_size(this_view_camera, global.glob_resolutionW, global.glob_resolutionH);

//Create Minigame presenatation layers
global.glob_layerMGameGUI = layer_create(global.glob_minDepth, "layer_mGameGui");
global.glob_layerMGamePresentation = layer_create(global.glob_minDepth + 10, "layer_mGamePresentation");
global.glob_layerMGameBg = layer_create(global.glob_minDepth + 20, "layer_mGameBg");