{
    "id": "72684c12-9592-47fa-8c72-90dbfa3bcceb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player",
    "eventList": [
        {
            "id": "217829a1-d667-4870-9695-4c4e6a41ca34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "72684c12-9592-47fa-8c72-90dbfa3bcceb"
        },
        {
            "id": "a8b30256-b818-4480-b751-f88ef68b3d69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "72684c12-9592-47fa-8c72-90dbfa3bcceb"
        },
        {
            "id": "e5c7d236-1680-4142-8062-c5a9e70ffabb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "72684c12-9592-47fa-8c72-90dbfa3bcceb"
        },
        {
            "id": "dba0789e-a6a9-4356-a2b6-9dca9640c9e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "72684c12-9592-47fa-8c72-90dbfa3bcceb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "622dd390-ccbc-47c2-b7a8-41bb79a8405a",
    "visible": true
}