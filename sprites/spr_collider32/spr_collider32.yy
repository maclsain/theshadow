{
    "id": "40da1dfa-32de-4aa5-b3a8-4206cc9c6c0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collider32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9db609ec-bb82-457a-92b5-2080dd51d66b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40da1dfa-32de-4aa5-b3a8-4206cc9c6c0e",
            "compositeImage": {
                "id": "18a6e6c3-5c17-491a-ae1b-5889e6b48095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db609ec-bb82-457a-92b5-2080dd51d66b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6dc3a10-486d-4dd1-8d20-d8650a5e2569",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db609ec-bb82-457a-92b5-2080dd51d66b",
                    "LayerId": "d744f559-fd24-4599-b160-841c0589facf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d744f559-fd24-4599-b160-841c0589facf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40da1dfa-32de-4aa5-b3a8-4206cc9c6c0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}