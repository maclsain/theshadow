{
    "id": "88365a41-fb39-445f-9430-62d9d2a024cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PWeight2_16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a7d690e-10f3-417b-951b-c9414426e7b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88365a41-fb39-445f-9430-62d9d2a024cd",
            "compositeImage": {
                "id": "69a58c03-768d-4f65-9a4c-5d21e11262b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a7d690e-10f3-417b-951b-c9414426e7b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24ad0bef-a544-4fe2-acb4-04161f58dfda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a7d690e-10f3-417b-951b-c9414426e7b5",
                    "LayerId": "53e0b898-0daf-461d-b258-fe74a2437eee"
                },
                {
                    "id": "30376e57-4345-498b-bf53-734aaa7eb2b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a7d690e-10f3-417b-951b-c9414426e7b5",
                    "LayerId": "fbe55bf0-bb09-43bf-9fd4-a354c003593f"
                },
                {
                    "id": "42d4e154-58bd-43db-9c80-edc987464dd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a7d690e-10f3-417b-951b-c9414426e7b5",
                    "LayerId": "f8072e52-2cfb-4506-ae96-ba9950fce95c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fbe55bf0-bb09-43bf-9fd4-a354c003593f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88365a41-fb39-445f-9430-62d9d2a024cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "f8072e52-2cfb-4506-ae96-ba9950fce95c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88365a41-fb39-445f-9430-62d9d2a024cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "53e0b898-0daf-461d-b258-fe74a2437eee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88365a41-fb39-445f-9430-62d9d2a024cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}