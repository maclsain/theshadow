{
    "id": "3e87b99f-ec79-456c-b660-022ffe1702dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0a90732-a932-4c4a-81e2-eb7fb462a0dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e87b99f-ec79-456c-b660-022ffe1702dc",
            "compositeImage": {
                "id": "18eacb18-1d56-4096-86c7-c9405f4bc97f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0a90732-a932-4c4a-81e2-eb7fb462a0dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a9ea5a3-992c-4508-a30a-fc5d3422594d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a90732-a932-4c4a-81e2-eb7fb462a0dc",
                    "LayerId": "4682a8b5-4d6f-43d1-94d7-db74e66791e6"
                },
                {
                    "id": "e2cf449f-d23c-4bc2-9145-84b300a01f12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a90732-a932-4c4a-81e2-eb7fb462a0dc",
                    "LayerId": "8a02dd50-77d8-4b30-8837-8d0fa393faaf"
                },
                {
                    "id": "b66c1bc2-0cb9-4bfe-91c9-35c45075f808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a90732-a932-4c4a-81e2-eb7fb462a0dc",
                    "LayerId": "2c25509d-39e8-4ac2-9e50-d8f8509a8bd9"
                }
            ]
        },
        {
            "id": "b9ad5b42-2a21-4497-a080-073d5bcb9c00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e87b99f-ec79-456c-b660-022ffe1702dc",
            "compositeImage": {
                "id": "307d1a1b-46dc-4329-80d8-f861845ad54d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9ad5b42-2a21-4497-a080-073d5bcb9c00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42e8dd37-7a0e-42c9-88a6-742f541567d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ad5b42-2a21-4497-a080-073d5bcb9c00",
                    "LayerId": "4682a8b5-4d6f-43d1-94d7-db74e66791e6"
                },
                {
                    "id": "cd67654e-569d-4042-9527-8ccf37faebe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ad5b42-2a21-4497-a080-073d5bcb9c00",
                    "LayerId": "8a02dd50-77d8-4b30-8837-8d0fa393faaf"
                },
                {
                    "id": "176b0c69-95e0-4a5c-b637-4dabee55efcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ad5b42-2a21-4497-a080-073d5bcb9c00",
                    "LayerId": "2c25509d-39e8-4ac2-9e50-d8f8509a8bd9"
                }
            ]
        },
        {
            "id": "4ad2d405-1a97-4898-ac2d-e4a88ed78d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e87b99f-ec79-456c-b660-022ffe1702dc",
            "compositeImage": {
                "id": "904d2dca-b79c-4e65-bd03-50e82f75334d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ad2d405-1a97-4898-ac2d-e4a88ed78d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6902a556-2ae3-41bd-a10b-b8ac94e4118c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ad2d405-1a97-4898-ac2d-e4a88ed78d23",
                    "LayerId": "4682a8b5-4d6f-43d1-94d7-db74e66791e6"
                },
                {
                    "id": "f962c733-5819-4fe7-be5e-cf536d0d4aad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ad2d405-1a97-4898-ac2d-e4a88ed78d23",
                    "LayerId": "8a02dd50-77d8-4b30-8837-8d0fa393faaf"
                },
                {
                    "id": "fcbeea22-a980-4a00-8357-06b02d8a83f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ad2d405-1a97-4898-ac2d-e4a88ed78d23",
                    "LayerId": "2c25509d-39e8-4ac2-9e50-d8f8509a8bd9"
                }
            ]
        },
        {
            "id": "d7754541-8b4d-4384-974e-f6c6e4f25d17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e87b99f-ec79-456c-b660-022ffe1702dc",
            "compositeImage": {
                "id": "c8645c56-5b08-47de-b67d-4c322d129c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7754541-8b4d-4384-974e-f6c6e4f25d17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454032ac-cdd4-4728-9eb2-b94d3d036da7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7754541-8b4d-4384-974e-f6c6e4f25d17",
                    "LayerId": "4682a8b5-4d6f-43d1-94d7-db74e66791e6"
                },
                {
                    "id": "61c5aae6-ac9e-4ba5-9c50-2957c6bbb10b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7754541-8b4d-4384-974e-f6c6e4f25d17",
                    "LayerId": "8a02dd50-77d8-4b30-8837-8d0fa393faaf"
                },
                {
                    "id": "1a9e98a3-e57e-4bf9-b00d-e620d562f953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7754541-8b4d-4384-974e-f6c6e4f25d17",
                    "LayerId": "2c25509d-39e8-4ac2-9e50-d8f8509a8bd9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4682a8b5-4d6f-43d1-94d7-db74e66791e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e87b99f-ec79-456c-b660-022ffe1702dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8a02dd50-77d8-4b30-8837-8d0fa393faaf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e87b99f-ec79-456c-b660-022ffe1702dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "2c25509d-39e8-4ac2-9e50-d8f8509a8bd9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e87b99f-ec79-456c-b660-022ffe1702dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 2",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}