{
    "id": "fc702c58-ef91-4345-8732-bb4c1e2ffdbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anim_PLeft_64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5daf8491-ebcd-4973-aba2-d7479a86f15b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc702c58-ef91-4345-8732-bb4c1e2ffdbc",
            "compositeImage": {
                "id": "b23c43a2-b69f-4491-9b6e-3419c54db66e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5daf8491-ebcd-4973-aba2-d7479a86f15b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4d44ac7-6b3e-44e7-992c-58ce4ede0bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5daf8491-ebcd-4973-aba2-d7479a86f15b",
                    "LayerId": "252ae3f3-1422-438c-9bf3-4328ed3f26f6"
                }
            ]
        },
        {
            "id": "bc7431c8-38dd-4e94-a717-6b0bd698ebfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc702c58-ef91-4345-8732-bb4c1e2ffdbc",
            "compositeImage": {
                "id": "cd0e37ee-3211-46a6-a297-1db50891e235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc7431c8-38dd-4e94-a717-6b0bd698ebfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a947c344-0799-4cc5-a6b4-942ef69953d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc7431c8-38dd-4e94-a717-6b0bd698ebfd",
                    "LayerId": "252ae3f3-1422-438c-9bf3-4328ed3f26f6"
                }
            ]
        },
        {
            "id": "57a61dc2-af2e-40eb-a400-0240b663b646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc702c58-ef91-4345-8732-bb4c1e2ffdbc",
            "compositeImage": {
                "id": "b6294e50-d3ed-4784-9c15-679ee8a7fd0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a61dc2-af2e-40eb-a400-0240b663b646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfd66ea0-c197-4dd5-ace0-737d3c934ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a61dc2-af2e-40eb-a400-0240b663b646",
                    "LayerId": "252ae3f3-1422-438c-9bf3-4328ed3f26f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "252ae3f3-1422-438c-9bf3-4328ed3f26f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc702c58-ef91-4345-8732-bb4c1e2ffdbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}