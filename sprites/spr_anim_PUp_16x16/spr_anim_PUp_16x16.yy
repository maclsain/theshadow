{
    "id": "ebd6ddf2-4bc5-4484-9bd4-608be50a9d08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anim_PUp_16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e84302df-49b3-4bbb-b4a3-cd8478386be4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebd6ddf2-4bc5-4484-9bd4-608be50a9d08",
            "compositeImage": {
                "id": "2d7a4466-cada-4486-a624-d412022e2270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84302df-49b3-4bbb-b4a3-cd8478386be4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd015db7-e02b-4856-8a41-a9e627ea6797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84302df-49b3-4bbb-b4a3-cd8478386be4",
                    "LayerId": "b57a4e2f-bd40-4cf5-812c-75fec85f2e8f"
                },
                {
                    "id": "8f433f0b-ff50-4bae-833e-bbf9037a67dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84302df-49b3-4bbb-b4a3-cd8478386be4",
                    "LayerId": "e98e0c90-179c-49af-9b62-307a9f91798e"
                },
                {
                    "id": "ee719c2a-c429-4081-b4bb-f1f253d41e5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84302df-49b3-4bbb-b4a3-cd8478386be4",
                    "LayerId": "850ce38c-6bb1-4f3b-bfcd-311ce2a0c2bb"
                }
            ]
        },
        {
            "id": "a916b251-d7cc-479d-b268-89e2fc0d41d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebd6ddf2-4bc5-4484-9bd4-608be50a9d08",
            "compositeImage": {
                "id": "0ef9e124-c8cb-4fab-bf1d-390d173538a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a916b251-d7cc-479d-b268-89e2fc0d41d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71be636a-fc7e-4279-bd25-0c12ff414c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a916b251-d7cc-479d-b268-89e2fc0d41d7",
                    "LayerId": "850ce38c-6bb1-4f3b-bfcd-311ce2a0c2bb"
                },
                {
                    "id": "3455001e-2c31-4168-8ca8-1b8263d6e359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a916b251-d7cc-479d-b268-89e2fc0d41d7",
                    "LayerId": "e98e0c90-179c-49af-9b62-307a9f91798e"
                },
                {
                    "id": "77084e42-1234-42b6-a30c-1da117bff8d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a916b251-d7cc-479d-b268-89e2fc0d41d7",
                    "LayerId": "b57a4e2f-bd40-4cf5-812c-75fec85f2e8f"
                }
            ]
        },
        {
            "id": "e69b6ff8-290a-4c41-88db-c11f20d731cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebd6ddf2-4bc5-4484-9bd4-608be50a9d08",
            "compositeImage": {
                "id": "e9fce615-1497-46e0-94a0-6ce948dbc3b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e69b6ff8-290a-4c41-88db-c11f20d731cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b9f6f07-7abf-4708-8c35-e2ba650a6ec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69b6ff8-290a-4c41-88db-c11f20d731cf",
                    "LayerId": "b57a4e2f-bd40-4cf5-812c-75fec85f2e8f"
                },
                {
                    "id": "b116dfb8-7f5b-45c5-b023-3bba4c22229d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69b6ff8-290a-4c41-88db-c11f20d731cf",
                    "LayerId": "e98e0c90-179c-49af-9b62-307a9f91798e"
                },
                {
                    "id": "c0da377b-83e1-429e-a1d3-c5395c406777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69b6ff8-290a-4c41-88db-c11f20d731cf",
                    "LayerId": "850ce38c-6bb1-4f3b-bfcd-311ce2a0c2bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "850ce38c-6bb1-4f3b-bfcd-311ce2a0c2bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebd6ddf2-4bc5-4484-9bd4-608be50a9d08",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e98e0c90-179c-49af-9b62-307a9f91798e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebd6ddf2-4bc5-4484-9bd4-608be50a9d08",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b57a4e2f-bd40-4cf5-812c-75fec85f2e8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebd6ddf2-4bc5-4484-9bd4-608be50a9d08",
            "blendMode": 0,
            "isLocked": true,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}