{
    "id": "c5b88e49-97af-479b-80ee-ee68d603ca65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dumbbell2_16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28bfbb7b-0531-4c8b-afdb-5c84c14c6c07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5b88e49-97af-479b-80ee-ee68d603ca65",
            "compositeImage": {
                "id": "09f809e5-a61e-4178-be29-d55bb032d1f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28bfbb7b-0531-4c8b-afdb-5c84c14c6c07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bd7ce79-144d-4af5-857d-0fdf8bca2c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28bfbb7b-0531-4c8b-afdb-5c84c14c6c07",
                    "LayerId": "117dfa65-68c1-4c2a-b25c-d9d7ad163f7b"
                },
                {
                    "id": "d0615b48-0b67-4f67-84a2-38db3ff1ff01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28bfbb7b-0531-4c8b-afdb-5c84c14c6c07",
                    "LayerId": "d183df2b-54e7-4be0-9a3f-59b7224d8ff2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "117dfa65-68c1-4c2a-b25c-d9d7ad163f7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5b88e49-97af-479b-80ee-ee68d603ca65",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d183df2b-54e7-4be0-9a3f-59b7224d8ff2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5b88e49-97af-479b-80ee-ee68d603ca65",
            "blendMode": 0,
            "isLocked": false,
            "name": "bg",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}