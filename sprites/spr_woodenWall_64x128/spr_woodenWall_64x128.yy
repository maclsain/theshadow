{
    "id": "042db0d1-6227-4eb3-88e3-87fdc51da21b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_woodenWall_64x128",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfe12b5d-55f7-4f76-b4fd-aeefefa7f11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "042db0d1-6227-4eb3-88e3-87fdc51da21b",
            "compositeImage": {
                "id": "ada4a3f3-566a-42a5-9392-4fb75ce2a874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfe12b5d-55f7-4f76-b4fd-aeefefa7f11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e53b17f-4631-4c61-bf51-5fed091f0118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfe12b5d-55f7-4f76-b4fd-aeefefa7f11d",
                    "LayerId": "65ee402b-a99c-4ff1-a860-858a49a807a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "65ee402b-a99c-4ff1-a860-858a49a807a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "042db0d1-6227-4eb3-88e3-87fdc51da21b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 64
}