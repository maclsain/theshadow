{
    "id": "594d8c3a-39fb-4403-bff2-caabe33ddcaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bed_64x128",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a8e95e0-cd0e-4e49-9100-f0d8ad47ca8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "594d8c3a-39fb-4403-bff2-caabe33ddcaf",
            "compositeImage": {
                "id": "53837ac7-cb1d-43f8-839e-9d6206c9c86c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a8e95e0-cd0e-4e49-9100-f0d8ad47ca8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "328e40ce-c144-4dca-9f7a-682de1cf4910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a8e95e0-cd0e-4e49-9100-f0d8ad47ca8f",
                    "LayerId": "478ec428-2365-4c0e-870e-bee1dec8d5ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "478ec428-2365-4c0e-870e-bee1dec8d5ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "594d8c3a-39fb-4403-bff2-caabe33ddcaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}