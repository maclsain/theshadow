{
    "id": "3f99fe96-1f0f-4997-a9ba-4f4750484684",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hardwood2_64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb7cd5e5-3171-4857-a6ec-ea5617080ba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f99fe96-1f0f-4997-a9ba-4f4750484684",
            "compositeImage": {
                "id": "2de68b37-6e0b-43a4-9cbe-d11e5af70b62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb7cd5e5-3171-4857-a6ec-ea5617080ba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3122c922-6b8c-41cd-8349-537d87fb6eb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb7cd5e5-3171-4857-a6ec-ea5617080ba8",
                    "LayerId": "43e28b1f-ae4f-4f86-b064-041d6d513fa1"
                },
                {
                    "id": "6e818ec4-40ea-4859-9085-b380f2362262",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb7cd5e5-3171-4857-a6ec-ea5617080ba8",
                    "LayerId": "5062ba6b-827b-4d8a-826c-6d2daf5ec713"
                },
                {
                    "id": "eb126025-ef14-4da3-aca4-35ad04ea4c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb7cd5e5-3171-4857-a6ec-ea5617080ba8",
                    "LayerId": "de49109e-f32e-4e5e-b762-86896de758ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "43e28b1f-ae4f-4f86-b064-041d6d513fa1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f99fe96-1f0f-4997-a9ba-4f4750484684",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5062ba6b-827b-4d8a-826c-6d2daf5ec713",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f99fe96-1f0f-4997-a9ba-4f4750484684",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "de49109e-f32e-4e5e-b762-86896de758ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f99fe96-1f0f-4997-a9ba-4f4750484684",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}