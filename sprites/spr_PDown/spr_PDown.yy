{
    "id": "622dd390-ccbc-47c2-b7a8-41bb79a8405a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "deba2503-f5f2-4613-9b26-51fe21a3f944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622dd390-ccbc-47c2-b7a8-41bb79a8405a",
            "compositeImage": {
                "id": "994438f3-85f8-40b7-bad8-1dcb10719d79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deba2503-f5f2-4613-9b26-51fe21a3f944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "702ae00d-fcc1-476e-96fd-852baa429907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deba2503-f5f2-4613-9b26-51fe21a3f944",
                    "LayerId": "335fc794-a22b-4fe6-9f90-4b75504f2138"
                },
                {
                    "id": "d0bb7dfc-9fe5-4f31-8952-9e18375c8c18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deba2503-f5f2-4613-9b26-51fe21a3f944",
                    "LayerId": "03d7b425-0f8b-4fad-9e2b-0b76df695eee"
                },
                {
                    "id": "a1ba719e-6f95-43da-b905-0bff1948bab2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deba2503-f5f2-4613-9b26-51fe21a3f944",
                    "LayerId": "6404ef65-10ad-4167-baeb-e6663dc31683"
                }
            ]
        },
        {
            "id": "5f591f18-d1db-4164-b556-583b6cf969d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622dd390-ccbc-47c2-b7a8-41bb79a8405a",
            "compositeImage": {
                "id": "eb983e8a-8366-4e3e-b96b-7403495ec7b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f591f18-d1db-4164-b556-583b6cf969d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "231f16e6-eb6b-4681-b8e5-e0c7999b8aa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f591f18-d1db-4164-b556-583b6cf969d6",
                    "LayerId": "6404ef65-10ad-4167-baeb-e6663dc31683"
                },
                {
                    "id": "86ef24d8-47a2-4f0f-bee8-5673ffa6108c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f591f18-d1db-4164-b556-583b6cf969d6",
                    "LayerId": "03d7b425-0f8b-4fad-9e2b-0b76df695eee"
                },
                {
                    "id": "bceaa67f-6571-4ccf-8a75-75b767d456bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f591f18-d1db-4164-b556-583b6cf969d6",
                    "LayerId": "335fc794-a22b-4fe6-9f90-4b75504f2138"
                }
            ]
        },
        {
            "id": "5500a525-251b-429c-8088-fed2ad16ae63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622dd390-ccbc-47c2-b7a8-41bb79a8405a",
            "compositeImage": {
                "id": "26a69d08-5b25-4a9e-b6f1-f5fd7085b113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5500a525-251b-429c-8088-fed2ad16ae63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78f783c2-4ff6-4cf1-be21-2ff7435b148e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5500a525-251b-429c-8088-fed2ad16ae63",
                    "LayerId": "6404ef65-10ad-4167-baeb-e6663dc31683"
                },
                {
                    "id": "1d4e8191-e04f-475f-a5f7-e3e71f90d3dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5500a525-251b-429c-8088-fed2ad16ae63",
                    "LayerId": "03d7b425-0f8b-4fad-9e2b-0b76df695eee"
                },
                {
                    "id": "9668d7aa-f18d-4fed-acc1-60e94200f300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5500a525-251b-429c-8088-fed2ad16ae63",
                    "LayerId": "335fc794-a22b-4fe6-9f90-4b75504f2138"
                }
            ]
        },
        {
            "id": "f288d959-ded6-4809-a21e-785795868183",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "622dd390-ccbc-47c2-b7a8-41bb79a8405a",
            "compositeImage": {
                "id": "6155f30d-87e6-4589-80ba-b617a4fcb37d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f288d959-ded6-4809-a21e-785795868183",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41a91d71-6748-4aab-a15a-a9482d616427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f288d959-ded6-4809-a21e-785795868183",
                    "LayerId": "6404ef65-10ad-4167-baeb-e6663dc31683"
                },
                {
                    "id": "2a145f4c-1f76-461f-b7f7-e21e61d3ca9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f288d959-ded6-4809-a21e-785795868183",
                    "LayerId": "03d7b425-0f8b-4fad-9e2b-0b76df695eee"
                },
                {
                    "id": "fca91db3-ce8b-4b5a-9fd9-da9472d21496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f288d959-ded6-4809-a21e-785795868183",
                    "LayerId": "335fc794-a22b-4fe6-9f90-4b75504f2138"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6404ef65-10ad-4167-baeb-e6663dc31683",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "622dd390-ccbc-47c2-b7a8-41bb79a8405a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "03d7b425-0f8b-4fad-9e2b-0b76df695eee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "622dd390-ccbc-47c2-b7a8-41bb79a8405a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "335fc794-a22b-4fe6-9f90-4b75504f2138",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "622dd390-ccbc-47c2-b7a8-41bb79a8405a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}