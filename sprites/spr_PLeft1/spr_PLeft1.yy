{
    "id": "b314813a-5532-4255-ae93-9bac31f99699",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PLeft1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4880322e-1288-4c51-9737-74fad4218e83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b314813a-5532-4255-ae93-9bac31f99699",
            "compositeImage": {
                "id": "ab5289b2-ce18-4587-8583-b6073cf7a2f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4880322e-1288-4c51-9737-74fad4218e83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c902d892-0148-471e-bc5e-54b504e46e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4880322e-1288-4c51-9737-74fad4218e83",
                    "LayerId": "eb915bc0-8ba3-44ac-8b80-351b962943aa"
                },
                {
                    "id": "61d4493b-538a-4c03-9c60-2f48283aafb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4880322e-1288-4c51-9737-74fad4218e83",
                    "LayerId": "464050f8-b566-4519-8e38-75b610649ba7"
                },
                {
                    "id": "10e70513-7a16-450e-8595-65cfe81da58b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4880322e-1288-4c51-9737-74fad4218e83",
                    "LayerId": "7c36fc79-8b3c-4dda-8e16-196946bea8f1"
                }
            ]
        },
        {
            "id": "b1aefae9-0e86-42ad-bef2-97393a770096",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b314813a-5532-4255-ae93-9bac31f99699",
            "compositeImage": {
                "id": "fd313ebd-1a81-494b-8156-04446d449970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1aefae9-0e86-42ad-bef2-97393a770096",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bc07cf3-d7be-4bbc-b044-a80da1a84fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1aefae9-0e86-42ad-bef2-97393a770096",
                    "LayerId": "eb915bc0-8ba3-44ac-8b80-351b962943aa"
                },
                {
                    "id": "e1204510-6848-4334-b15e-0481257e44e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1aefae9-0e86-42ad-bef2-97393a770096",
                    "LayerId": "464050f8-b566-4519-8e38-75b610649ba7"
                },
                {
                    "id": "f42fc25c-e347-40b3-a544-3dc9bbf89bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1aefae9-0e86-42ad-bef2-97393a770096",
                    "LayerId": "7c36fc79-8b3c-4dda-8e16-196946bea8f1"
                }
            ]
        },
        {
            "id": "5a3df6fd-422a-4796-ae29-421cab29c350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b314813a-5532-4255-ae93-9bac31f99699",
            "compositeImage": {
                "id": "3c864bff-4b48-4ec9-86c4-38310cdc087f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a3df6fd-422a-4796-ae29-421cab29c350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05aaab3f-4407-48bb-aee2-42c4b3f84cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a3df6fd-422a-4796-ae29-421cab29c350",
                    "LayerId": "eb915bc0-8ba3-44ac-8b80-351b962943aa"
                },
                {
                    "id": "fd880cdf-85db-44bc-9919-12b8bd729213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a3df6fd-422a-4796-ae29-421cab29c350",
                    "LayerId": "464050f8-b566-4519-8e38-75b610649ba7"
                },
                {
                    "id": "f92c32e3-e8f4-4cd2-8a53-36d727718282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a3df6fd-422a-4796-ae29-421cab29c350",
                    "LayerId": "7c36fc79-8b3c-4dda-8e16-196946bea8f1"
                }
            ]
        },
        {
            "id": "4c47f6d9-64aa-4ffc-87dd-052e1e772886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b314813a-5532-4255-ae93-9bac31f99699",
            "compositeImage": {
                "id": "b77a473c-82fc-4728-88a9-4554d3fd8572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c47f6d9-64aa-4ffc-87dd-052e1e772886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7401af5a-d11b-45cb-ab7d-f8b55b8bbb8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c47f6d9-64aa-4ffc-87dd-052e1e772886",
                    "LayerId": "eb915bc0-8ba3-44ac-8b80-351b962943aa"
                },
                {
                    "id": "27555f80-7a8e-4633-b001-438950ada01a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c47f6d9-64aa-4ffc-87dd-052e1e772886",
                    "LayerId": "464050f8-b566-4519-8e38-75b610649ba7"
                },
                {
                    "id": "74bc2137-7652-45c5-80e4-3c806b60ff81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c47f6d9-64aa-4ffc-87dd-052e1e772886",
                    "LayerId": "7c36fc79-8b3c-4dda-8e16-196946bea8f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "eb915bc0-8ba3-44ac-8b80-351b962943aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b314813a-5532-4255-ae93-9bac31f99699",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "464050f8-b566-4519-8e38-75b610649ba7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b314813a-5532-4255-ae93-9bac31f99699",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7c36fc79-8b3c-4dda-8e16-196946bea8f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b314813a-5532-4255-ae93-9bac31f99699",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 2",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}