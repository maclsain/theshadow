{
    "id": "b9902a0c-e80b-4727-8fba-56adaf80cfa5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hardwood_64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b22cd200-06c2-4129-bdb8-4ad328194064",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9902a0c-e80b-4727-8fba-56adaf80cfa5",
            "compositeImage": {
                "id": "84843b94-dd9f-44d3-88af-28c082221037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22cd200-06c2-4129-bdb8-4ad328194064",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f4c053c-0d99-424b-817f-a9f2176eb210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22cd200-06c2-4129-bdb8-4ad328194064",
                    "LayerId": "5615eafa-f2c7-494a-a780-2e0311b97b8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5615eafa-f2c7-494a-a780-2e0311b97b8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9902a0c-e80b-4727-8fba-56adaf80cfa5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}