{
    "id": "b605a388-74ec-43f2-b28f-8ad766e0b9b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Master_64x64Tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4791,
    "bbox_left": 0,
    "bbox_right": 7195,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a7821fe-9399-4e19-99a4-99ac84ae6dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b605a388-74ec-43f2-b28f-8ad766e0b9b4",
            "compositeImage": {
                "id": "d63174a9-19c7-4f87-9e20-5d9ffd8594f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a7821fe-9399-4e19-99a4-99ac84ae6dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b0a37a-5012-4211-bdd6-c10fe04ede57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a7821fe-9399-4e19-99a4-99ac84ae6dad",
                    "LayerId": "f5c3b7ca-d9aa-41cc-99f9-f19ccc354d98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4792,
    "layers": [
        {
            "id": "f5c3b7ca-d9aa-41cc-99f9-f19ccc354d98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b605a388-74ec-43f2-b28f-8ad766e0b9b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7196,
    "xorig": 303,
    "yorig": 428
}