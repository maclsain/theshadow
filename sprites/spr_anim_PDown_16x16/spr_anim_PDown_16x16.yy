{
    "id": "39af95db-8096-4e16-aed2-ce012d14fb78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anim_PDown_16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50a3d26b-b225-413f-b907-02c2b1e5d6db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39af95db-8096-4e16-aed2-ce012d14fb78",
            "compositeImage": {
                "id": "6f1f4803-4e66-4d1e-b1c6-0c6d28624699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50a3d26b-b225-413f-b907-02c2b1e5d6db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f618eff-8adc-4088-acef-60ea4569afd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50a3d26b-b225-413f-b907-02c2b1e5d6db",
                    "LayerId": "8663d60e-ffc3-4f42-8e43-078dd3f2aca1"
                },
                {
                    "id": "714380d4-afb7-4b3d-8dd5-05a3c16b7dff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50a3d26b-b225-413f-b907-02c2b1e5d6db",
                    "LayerId": "fc50ad54-505c-46eb-804e-6b61c3441ab0"
                }
            ]
        },
        {
            "id": "86fa20dd-81d5-47d0-9f7f-9ed652bd0347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39af95db-8096-4e16-aed2-ce012d14fb78",
            "compositeImage": {
                "id": "19337a8e-491e-4373-81a8-cd5293f9abf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86fa20dd-81d5-47d0-9f7f-9ed652bd0347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0c86697-714f-4c09-9a78-ad3ed382bef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86fa20dd-81d5-47d0-9f7f-9ed652bd0347",
                    "LayerId": "8663d60e-ffc3-4f42-8e43-078dd3f2aca1"
                },
                {
                    "id": "349a3315-09bb-4a92-b2b3-beddf54501c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86fa20dd-81d5-47d0-9f7f-9ed652bd0347",
                    "LayerId": "fc50ad54-505c-46eb-804e-6b61c3441ab0"
                }
            ]
        },
        {
            "id": "2d5b4102-0330-46a1-aeb2-b351bd72dab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39af95db-8096-4e16-aed2-ce012d14fb78",
            "compositeImage": {
                "id": "6654dfe1-0bf8-4cfe-bb82-a18fcadc1f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d5b4102-0330-46a1-aeb2-b351bd72dab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a784777-d8c1-494f-b99f-56ffb3e1d106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d5b4102-0330-46a1-aeb2-b351bd72dab9",
                    "LayerId": "8663d60e-ffc3-4f42-8e43-078dd3f2aca1"
                },
                {
                    "id": "2cfef7ec-23cd-4006-aca9-50bde1cf8796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d5b4102-0330-46a1-aeb2-b351bd72dab9",
                    "LayerId": "fc50ad54-505c-46eb-804e-6b61c3441ab0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8663d60e-ffc3-4f42-8e43-078dd3f2aca1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39af95db-8096-4e16-aed2-ce012d14fb78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fc50ad54-505c-46eb-804e-6b61c3441ab0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39af95db-8096-4e16-aed2-ce012d14fb78",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}