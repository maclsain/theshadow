{
    "id": "5a3a607e-c09b-4935-87eb-39fb44c92fb5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Game",
    "eventList": [
        {
            "id": "b0e8211b-47ee-4a0d-b7fb-d40c0d58fc69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "5a3a607e-c09b-4935-87eb-39fb44c92fb5"
        },
        {
            "id": "730e5d7e-f4da-468e-b515-c3cec776258c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a3a607e-c09b-4935-87eb-39fb44c92fb5"
        },
        {
            "id": "0bc4e251-7c27-4a38-85c5-0988c3e7f72f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5a3a607e-c09b-4935-87eb-39fb44c92fb5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}