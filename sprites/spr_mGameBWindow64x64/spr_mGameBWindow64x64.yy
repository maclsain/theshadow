{
    "id": "52caade3-6561-4456-8de7-1a27436bf115",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mGameBWindow64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fbf66b4-1a38-4f68-95da-982a72e1273c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52caade3-6561-4456-8de7-1a27436bf115",
            "compositeImage": {
                "id": "7a07ceeb-cb76-4f38-932d-7343f391c79a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fbf66b4-1a38-4f68-95da-982a72e1273c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45107ac9-cbb4-4034-b5ea-ca9ca2e35137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fbf66b4-1a38-4f68-95da-982a72e1273c",
                    "LayerId": "12011f3d-56b2-42a2-b1ba-03a59369cd11"
                },
                {
                    "id": "bc66b0f0-e93b-4e01-b42b-b7393ba3131a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fbf66b4-1a38-4f68-95da-982a72e1273c",
                    "LayerId": "1c1182b4-322d-402b-9d03-72c25a47c060"
                }
            ]
        },
        {
            "id": "69a29d31-9bc4-4dba-aa03-41c9c042182b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52caade3-6561-4456-8de7-1a27436bf115",
            "compositeImage": {
                "id": "949484c7-8547-4a00-a2d0-a8e056ea7d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69a29d31-9bc4-4dba-aa03-41c9c042182b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c752001-43ad-4f9f-bdd7-fe09b755d222",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a29d31-9bc4-4dba-aa03-41c9c042182b",
                    "LayerId": "12011f3d-56b2-42a2-b1ba-03a59369cd11"
                },
                {
                    "id": "63188183-13d7-4154-844d-ae31d9434c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a29d31-9bc4-4dba-aa03-41c9c042182b",
                    "LayerId": "1c1182b4-322d-402b-9d03-72c25a47c060"
                }
            ]
        },
        {
            "id": "ffa96ff5-1e6c-464b-81c3-3afc4883cef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52caade3-6561-4456-8de7-1a27436bf115",
            "compositeImage": {
                "id": "7f29a146-a47d-47b6-ab93-3b9ee6f93f8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa96ff5-1e6c-464b-81c3-3afc4883cef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c30a7e40-9924-490a-b193-63c8b3fa7ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa96ff5-1e6c-464b-81c3-3afc4883cef1",
                    "LayerId": "12011f3d-56b2-42a2-b1ba-03a59369cd11"
                },
                {
                    "id": "919dfaf7-e6de-40b2-91f2-8e173487047d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa96ff5-1e6c-464b-81c3-3afc4883cef1",
                    "LayerId": "1c1182b4-322d-402b-9d03-72c25a47c060"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "12011f3d-56b2-42a2-b1ba-03a59369cd11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52caade3-6561-4456-8de7-1a27436bf115",
            "blendMode": 0,
            "isLocked": false,
            "name": "Frame",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1c1182b4-322d-402b-9d03-72c25a47c060",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52caade3-6561-4456-8de7-1a27436bf115",
            "blendMode": 0,
            "isLocked": false,
            "name": "bg",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}