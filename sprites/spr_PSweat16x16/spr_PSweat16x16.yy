{
    "id": "67b17a76-6059-47e0-b644-b413bff3e15d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PSweat16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecc11880-af9b-486f-93e4-ac86bda369e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b17a76-6059-47e0-b644-b413bff3e15d",
            "compositeImage": {
                "id": "2203a69f-e618-4994-b7fb-84ee30c83ed9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc11880-af9b-486f-93e4-ac86bda369e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41f62487-bd8a-4c27-beb4-53469c9f454c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc11880-af9b-486f-93e4-ac86bda369e5",
                    "LayerId": "57a9e5d7-2806-4112-89d9-a35ef9d3b148"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "57a9e5d7-2806-4112-89d9-a35ef9d3b148",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67b17a76-6059-47e0-b644-b413bff3e15d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 296,
    "yorig": 12
}