{
    "id": "b00a68a0-af46-4876-9507-650f50c06392",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hardwood2_16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01745f77-6b29-41da-a51c-96d0cc6efc71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b00a68a0-af46-4876-9507-650f50c06392",
            "compositeImage": {
                "id": "1f8250f4-0660-4ad2-bcd3-6044c5947dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01745f77-6b29-41da-a51c-96d0cc6efc71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b1d06c-5955-4098-97e2-2b75c93fe26d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01745f77-6b29-41da-a51c-96d0cc6efc71",
                    "LayerId": "11e2e28a-f1b8-4c7e-8bd0-1c6b19f33a41"
                },
                {
                    "id": "57419060-fd59-4ead-b8e7-47701ba74c3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01745f77-6b29-41da-a51c-96d0cc6efc71",
                    "LayerId": "3cbf5b29-d9d9-469b-beab-25ccf2073378"
                },
                {
                    "id": "59d21f76-305d-4a66-81d5-5e17f00c5b67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01745f77-6b29-41da-a51c-96d0cc6efc71",
                    "LayerId": "e083f2e5-449c-4e09-8bca-2dfc6c7e40a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3cbf5b29-d9d9-469b-beab-25ccf2073378",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b00a68a0-af46-4876-9507-650f50c06392",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e083f2e5-449c-4e09-8bca-2dfc6c7e40a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b00a68a0-af46-4876-9507-650f50c06392",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "11e2e28a-f1b8-4c7e-8bd0-1c6b19f33a41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b00a68a0-af46-4876-9507-650f50c06392",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}