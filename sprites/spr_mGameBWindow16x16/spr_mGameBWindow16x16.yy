{
    "id": "b435fcea-d448-4f8c-a3ce-c9d29ea456e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mGameBWindow16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bca27a06-db4f-41b6-af58-abf8b74ae9d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b435fcea-d448-4f8c-a3ce-c9d29ea456e6",
            "compositeImage": {
                "id": "9b6f0b43-7559-42cb-9338-5facd2906113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca27a06-db4f-41b6-af58-abf8b74ae9d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9e2c6ce-183e-43a6-a942-66aa71999bab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca27a06-db4f-41b6-af58-abf8b74ae9d2",
                    "LayerId": "71715621-eb9e-4db4-a1bd-75a738352993"
                },
                {
                    "id": "0ae8bd88-a364-48d3-bd30-39c04d534384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca27a06-db4f-41b6-af58-abf8b74ae9d2",
                    "LayerId": "f215bc43-d747-48cf-82bd-361167a0cd21"
                }
            ]
        },
        {
            "id": "8d3cbd80-4e64-48db-ab8b-4acfb073ec78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b435fcea-d448-4f8c-a3ce-c9d29ea456e6",
            "compositeImage": {
                "id": "30eb93a7-f7d8-4fcb-acd2-ef4cfb6532fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3cbd80-4e64-48db-ab8b-4acfb073ec78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1257d291-ffec-427f-90e1-d9816479758c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3cbd80-4e64-48db-ab8b-4acfb073ec78",
                    "LayerId": "f215bc43-d747-48cf-82bd-361167a0cd21"
                },
                {
                    "id": "24da2086-9bc6-42f6-95f3-a81a5982fcd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3cbd80-4e64-48db-ab8b-4acfb073ec78",
                    "LayerId": "71715621-eb9e-4db4-a1bd-75a738352993"
                }
            ]
        },
        {
            "id": "891b9287-9299-4ba5-ac6c-6e2c99af8c3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b435fcea-d448-4f8c-a3ce-c9d29ea456e6",
            "compositeImage": {
                "id": "f196325c-8a12-46af-b1eb-741b049d9391",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "891b9287-9299-4ba5-ac6c-6e2c99af8c3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "523377b5-01d6-4f00-b803-c7967faf4d76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "891b9287-9299-4ba5-ac6c-6e2c99af8c3f",
                    "LayerId": "f215bc43-d747-48cf-82bd-361167a0cd21"
                },
                {
                    "id": "5d3e7b03-2295-47d0-abd0-41b4e410f502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "891b9287-9299-4ba5-ac6c-6e2c99af8c3f",
                    "LayerId": "71715621-eb9e-4db4-a1bd-75a738352993"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f215bc43-d747-48cf-82bd-361167a0cd21",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b435fcea-d448-4f8c-a3ce-c9d29ea456e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Frame",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "71715621-eb9e-4db4-a1bd-75a738352993",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b435fcea-d448-4f8c-a3ce-c9d29ea456e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "bg",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}