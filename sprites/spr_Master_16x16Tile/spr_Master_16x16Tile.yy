{
    "id": "5838fd13-c2a2-44eb-962a-96d2120e82cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Master_16x16Tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1197,
    "bbox_left": 0,
    "bbox_right": 1798,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c82dd9cf-1e4e-4ee4-a346-da1a1bc78b6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5838fd13-c2a2-44eb-962a-96d2120e82cc",
            "compositeImage": {
                "id": "761dff70-35f7-407d-b1a8-ecb491e6942a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c82dd9cf-1e4e-4ee4-a346-da1a1bc78b6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddafd097-89da-40e9-ae2c-5442f45a07ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c82dd9cf-1e4e-4ee4-a346-da1a1bc78b6f",
                    "LayerId": "2f99eb9f-b457-4215-aa9e-bff3657aac16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1198,
    "layers": [
        {
            "id": "2f99eb9f-b457-4215-aa9e-bff3657aac16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5838fd13-c2a2-44eb-962a-96d2120e82cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1799,
    "xorig": 303,
    "yorig": 428
}