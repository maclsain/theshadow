{
    "id": "9a266678-3ffe-4bc9-b25b-03751e62a89c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_collider64",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "5dd597f2-5cea-420e-909b-759b503c02a0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c855fadd-ba26-4cdd-8b1b-0bd40e1417c8",
    "visible": true
}