/// @description Main Player Controller


if(path_exists(playerPath))
{
	for(var i = 0; i < array_length_1d(arrMoveKeys); i++)
	{
		if(keyboard_check_pressed(arrMoveKeys[i]) or keyboard_check_released(arrMoveKeys[i]))
		{
			path_add_point(playerPath, x, y, playerSpeed);
			
		}
		
		if(keyboard_check(arrMoveKeys[i]) && !will_collide(i)){
				movement_player(i);
		}
		
		if(keyboard_check_released(arrMoveKeys[i])){
			if(arrMoveKeys[i] == vk_up or arrMoveKeys[i] == vk_down){
				YFlag = 0;
			}
		}
		
	}
}
else{
	playerPath = path_add();
}

