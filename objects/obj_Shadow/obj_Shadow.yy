{
    "id": "8a07a25e-8626-40cb-be25-f369481b325d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Shadow",
    "eventList": [
        {
            "id": "3e59bf0a-c997-4a97-9faa-3420b422f3c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a07a25e-8626-40cb-be25-f369481b325d"
        },
        {
            "id": "e66641bc-3a02-4ded-8dae-6548081b8291",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "8a07a25e-8626-40cb-be25-f369481b325d"
        },
        {
            "id": "51effd5c-219d-4926-b339-61e7248b55b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8a07a25e-8626-40cb-be25-f369481b325d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "b890a238-3845-46f4-b983-ba9179eb5e63",
    "visible": true
}