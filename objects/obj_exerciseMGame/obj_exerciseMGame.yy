{
    "id": "6f65f1e2-b76d-441d-a149-537ad817a1c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_exerciseMGame",
    "eventList": [
        {
            "id": "6da35f4f-a718-4479-b341-59b6986a506c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6f65f1e2-b76d-441d-a149-537ad817a1c6"
        },
        {
            "id": "e1251510-5f6e-42ac-b174-8b183e3f4596",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6f65f1e2-b76d-441d-a149-537ad817a1c6"
        },
        {
            "id": "749d64c9-b9ca-459c-a871-be8bc41e2c5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "6f65f1e2-b76d-441d-a149-537ad817a1c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "29b5b940-4e26-46dc-bb4a-0a2c9e807a23",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}