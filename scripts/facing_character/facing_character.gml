XDirection = argument0;
YDirection = argument1;
thisInstance = argument2;

var currentSprite = object_get_sprite(thisInstance);
var currentSpriteName = sprite_get_name(currentSprite);
var currentSpriteDirection = string_char_at(currentSprite,5);

switch (XDirection){
	case -1:
	  switch(YDirection){
		  case -1:
		    if(currentSpriteDirection == "U"){
				return "UP";
			}
			else if (currentSpriteDirection == "L"){
			    return "LEFT";
			}
			else{
				return "UP";
			}
		  break;
		  case 0:
		    return "LEFT";
		  break;
		  case 1:
		    if(currentSpriteDirection == "L"){
				return "LEFT";
			}
			else if (currentSpriteDirection == "D"){
				return "DOWN";
			}
			else{
				return "DOWN";
			}
		  break;
		  default:
			  return "NONE";
		  break;
	  }
	case 0:
	  switch (XDirection){
		  case -1:
		    return "LEFT";
		  break;
		  case 0:
		    return "NONE";
		  break;
		  case 1:
		    return "RIGHT";
		  break;
		  default:
		    return "NONE";
		  break;
	  }
	
	case 1:
	  switch(YDirection){
		  case -1:
		    if(currentSpriteDirection == "U"){
				return "UP";
			}
			else if (currentSpriteDirection == "R"){
			    return "RIGHT";	
			}
			else{
				return "UP";
			}
		  break;
		  case 0:
		    return "RIGHT";
		  break;
		  case 1:
		    if(currentSpriteDirection == "R"){
				return "RIGHT";
			}
			else if (currentSpriteDirection == "D"){
				return "DOWN";
			}
			else{
				return "DOWN";
			}
		  break;
		  default:
			  return "NONE";
		  break;
	  }
}
