{
    "id": "ffa52964-18f1-4ecc-bb63-62311cfad0e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PSweat32x32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f26b046b-43a1-4f11-ac08-6eb5e1026a72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffa52964-18f1-4ecc-bb63-62311cfad0e4",
            "compositeImage": {
                "id": "5668063f-1767-47f8-ab71-0643661d96e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f26b046b-43a1-4f11-ac08-6eb5e1026a72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2547302-23d3-44a0-ab46-cb46a5dc344f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f26b046b-43a1-4f11-ac08-6eb5e1026a72",
                    "LayerId": "230eb0bf-ca2a-4686-93a6-c31876dd40e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "230eb0bf-ca2a-4686-93a6-c31876dd40e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffa52964-18f1-4ecc-bb63-62311cfad0e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 31
}