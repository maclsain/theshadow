/*spr_mGameBWindow64x64 indexes: 
0 = top left corner (rot 90 = bottom left, rot 180 bottom right, rot 270 top right)
1 = top border (rot 90 = left border, rot 180 bottom border, rot 270 right border)
2 = empty space
*/

//1st row out of 4
draw_sprite_ext(spr_mGameBWindow64x64,0,bg_topRCornerX,bg_topRCornerY,1,1,0,-1,1);
draw_sprite_ext(spr_mGameBWindow64x64,1,bg_topRCornerX+64,bg_topRCornerY,1,1,0,-1,1);
draw_sprite_ext(spr_mGameBWindow64x64,0,bg_topRCornerX+128,bg_topRCornerY,1,1,270,-1,1);

//2nd row out of 4
draw_sprite_ext(spr_mGameBWindow64x64,1,bg_topRCornerX,bg_topRCornerY+64,1,1,90,-1,1);
draw_sprite_ext(spr_mGameBWindow64x64,2,bg_topRCornerX+64,bg_topRCornerY+64,1,1,0,-1,1);
draw_sprite_ext(spr_mGameBWindow64x64,1,bg_topRCornerX+128,bg_topRCornerY+64,1,1,270,-1,1);

//3rd row out of 4
draw_sprite_ext(spr_mGameBWindow64x64,1,bg_topRCornerX,bg_topRCornerY+128,1,1,90,-1,1);
draw_sprite_ext(spr_mGameBWindow64x64,2,bg_topRCornerX+64,bg_topRCornerY+128,1,1,0,-1,1);
draw_sprite_ext(spr_mGameBWindow64x64,1,bg_topRCornerX+128,bg_topRCornerY+128,1,1,270,-1,1);

//4th row out of 4
draw_sprite_ext(spr_mGameBWindow64x64,0,bg_topRCornerX,bg_topRCornerY+192,1,1,90,-1,1);
draw_sprite_ext(spr_mGameBWindow64x64,1,bg_topRCornerX+64,bg_topRCornerY+192,1,1,180,-1,1);
draw_sprite_ext(spr_mGameBWindow64x64,0,bg_topRCornerX+128,bg_topRCornerY+192,1,1,180,-1,1);