{
    "id": "abbbf50d-339b-43c3-b52d-ffa865434e93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "_old",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7cd6d07-a426-4f6f-bbec-62c6bbfabc20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abbbf50d-339b-43c3-b52d-ffa865434e93",
            "compositeImage": {
                "id": "73382b5e-2c1a-4441-8e2a-0eb2891f08b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7cd6d07-a426-4f6f-bbec-62c6bbfabc20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c26abf6-db03-46a5-b0aa-5208540d4195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7cd6d07-a426-4f6f-bbec-62c6bbfabc20",
                    "LayerId": "4d776142-2808-4017-85cc-4b83f219867c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4d776142-2808-4017-85cc-4b83f219867c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abbbf50d-339b-43c3-b52d-ffa865434e93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 64
}