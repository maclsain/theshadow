{
    "id": "08857e9f-1886-4e96-a571-4c76a8127435",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dumbbell16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f304e0dd-40bb-4103-8dc6-f7ea9cceaebf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08857e9f-1886-4e96-a571-4c76a8127435",
            "compositeImage": {
                "id": "a4e074d5-f99d-4cee-82c4-24a538354e45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f304e0dd-40bb-4103-8dc6-f7ea9cceaebf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc0ea776-10ec-4bfe-8fb6-21ce837b0855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f304e0dd-40bb-4103-8dc6-f7ea9cceaebf",
                    "LayerId": "f70322b6-5854-45c0-aeff-18480689d39a"
                },
                {
                    "id": "90357551-fe27-4d15-a21d-b01a7a8b027f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f304e0dd-40bb-4103-8dc6-f7ea9cceaebf",
                    "LayerId": "06985adb-a209-4ef6-a1de-c2a3e4a7b32c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f70322b6-5854-45c0-aeff-18480689d39a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08857e9f-1886-4e96-a571-4c76a8127435",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "06985adb-a209-4ef6-a1de-c2a3e4a7b32c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08857e9f-1886-4e96-a571-4c76a8127435",
            "blendMode": 0,
            "isLocked": false,
            "name": "bg",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 12,
    "yorig": 48
}