/// @description playerMovement(index)
var I = argument0;

var setFacing;

switch(playerSelf.arrMoveKeys[I]){
	case vk_up:
		playerSelf.y -= playerSpeed;
		id.YFlag = -1;
		//sprite_index = spr_PUp;
		image_speed = 0.5;
		break;
	case vk_right:
		playerSelf.x += playerSpeed;
		id.XFlag = 1;
		//sprite_index = spr_PRight;
		image_speed = 0.5;
		break;
	case vk_down:
		playerSelf.y += playerSpeed;
		id.YFlag = 1;
		//sprite_index = spr_PDown;
		image_speed = 0.5;
		break;
	case vk_left:
		playerSelf.x -= playerSpeed;
		id.XFlag = -1;
		//sprite_index = spr_PLeft;
		image_speed = 0.5;
		break;
	default:
	    image_speed = 0;
		break;
}

setFacing = facing_character(id.XFlag, id.YFlag, id);

switch(setFacing){
	case "UP":
	  sprite_index = spr_PUp;
	break;
	case "RIGHT":
	  sprite_index = spr_PRight;
	break;
	case "LEFT":
	  sprite_index = spr_PLeft;
	break;
	case "DOWN":
	  sprite_index = spr_PDown;
	break;
}