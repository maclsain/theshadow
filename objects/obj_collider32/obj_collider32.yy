{
    "id": "9a17fa02-765f-482d-b355-18e4fb178225",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_collider32",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "5dd597f2-5cea-420e-909b-759b503c02a0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "40da1dfa-32de-4aa5-b3a8-4206cc9c6c0e",
    "visible": true
}