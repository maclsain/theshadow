{
    "id": "e4851f46-2007-4951-b946-285f20d2fc45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anim_PUp_64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "790426d4-551c-449f-bead-35082b971384",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4851f46-2007-4951-b946-285f20d2fc45",
            "compositeImage": {
                "id": "b09c708d-eae6-49f9-8a01-6ec0c7da852a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "790426d4-551c-449f-bead-35082b971384",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43886f59-0fd5-41d9-a542-f6a0bf11466f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "790426d4-551c-449f-bead-35082b971384",
                    "LayerId": "874fedf7-a7c5-4579-bd5a-fa90f23781b5"
                },
                {
                    "id": "0346428f-aff9-4efc-b354-7c74a59a1fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "790426d4-551c-449f-bead-35082b971384",
                    "LayerId": "2ace8b32-4901-4c0d-ae3d-a69623211196"
                },
                {
                    "id": "0b544bc0-e9c3-44ee-843d-2caac7b62bab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "790426d4-551c-449f-bead-35082b971384",
                    "LayerId": "d35cd90d-e0ab-477b-b3ab-c528404a8446"
                }
            ]
        },
        {
            "id": "b22b3366-5ed5-4d0a-a8eb-354c6e1c90c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4851f46-2007-4951-b946-285f20d2fc45",
            "compositeImage": {
                "id": "7b118f48-307f-4448-af61-7fce131a8728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22b3366-5ed5-4d0a-a8eb-354c6e1c90c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb8f507-5a39-4eb8-a813-312ab8ee5d15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22b3366-5ed5-4d0a-a8eb-354c6e1c90c4",
                    "LayerId": "874fedf7-a7c5-4579-bd5a-fa90f23781b5"
                },
                {
                    "id": "a702a5ec-23dc-4e3f-a433-5a72d16c7b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22b3366-5ed5-4d0a-a8eb-354c6e1c90c4",
                    "LayerId": "2ace8b32-4901-4c0d-ae3d-a69623211196"
                },
                {
                    "id": "9bf49f9c-f88e-43fb-b4b6-fe364e300449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22b3366-5ed5-4d0a-a8eb-354c6e1c90c4",
                    "LayerId": "d35cd90d-e0ab-477b-b3ab-c528404a8446"
                }
            ]
        },
        {
            "id": "cdc550a5-9c2a-419b-a56c-c80acd3c5956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4851f46-2007-4951-b946-285f20d2fc45",
            "compositeImage": {
                "id": "fdd8fbea-a4ee-4b17-8887-068836d0bb83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdc550a5-9c2a-419b-a56c-c80acd3c5956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5991616-0cb1-40a6-a7ef-1846a8776ccb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdc550a5-9c2a-419b-a56c-c80acd3c5956",
                    "LayerId": "874fedf7-a7c5-4579-bd5a-fa90f23781b5"
                },
                {
                    "id": "e4b290f2-993d-4034-b7dc-d7d19f233ebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdc550a5-9c2a-419b-a56c-c80acd3c5956",
                    "LayerId": "2ace8b32-4901-4c0d-ae3d-a69623211196"
                },
                {
                    "id": "83f37be8-d43d-464c-bffb-ec11f4702c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdc550a5-9c2a-419b-a56c-c80acd3c5956",
                    "LayerId": "d35cd90d-e0ab-477b-b3ab-c528404a8446"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "874fedf7-a7c5-4579-bd5a-fa90f23781b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4851f46-2007-4951-b946-285f20d2fc45",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2ace8b32-4901-4c0d-ae3d-a69623211196",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4851f46-2007-4951-b946-285f20d2fc45",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d35cd90d-e0ab-477b-b3ab-c528404a8446",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4851f46-2007-4951-b946-285f20d2fc45",
            "blendMode": 0,
            "isLocked": true,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}