{
    "id": "b890a238-3845-46f4-b983-ba9179eb5e63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8524d26-d8ab-4e39-bbc3-cdb1494a4c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b890a238-3845-46f4-b983-ba9179eb5e63",
            "compositeImage": {
                "id": "f259b299-7e5c-4876-b6ec-14a565abe7fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8524d26-d8ab-4e39-bbc3-cdb1494a4c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ec71ed-682f-4781-b3fe-7c1dd7ccb28a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8524d26-d8ab-4e39-bbc3-cdb1494a4c2e",
                    "LayerId": "df54bd66-9a6d-4982-b630-44e9c34fd6d5"
                },
                {
                    "id": "48d91bd9-061c-4c52-87e2-c11cf8ac682c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8524d26-d8ab-4e39-bbc3-cdb1494a4c2e",
                    "LayerId": "b6352b42-b38c-4a66-9cb4-6d3000ff0d8a"
                },
                {
                    "id": "22504c21-1958-4a84-a71b-cf34ef7dc579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8524d26-d8ab-4e39-bbc3-cdb1494a4c2e",
                    "LayerId": "749bb517-b4fd-4d51-aa9c-303c5b965e1f"
                }
            ]
        },
        {
            "id": "3773add4-fc58-4ae1-b365-244fa2a97ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b890a238-3845-46f4-b983-ba9179eb5e63",
            "compositeImage": {
                "id": "828cf8c9-c134-453e-a07b-87ebda70b14b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3773add4-fc58-4ae1-b365-244fa2a97ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39db93c7-2669-4afb-a706-5178378a9ac9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3773add4-fc58-4ae1-b365-244fa2a97ed2",
                    "LayerId": "df54bd66-9a6d-4982-b630-44e9c34fd6d5"
                },
                {
                    "id": "14ac081e-26a8-4fac-948f-3805f7a89014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3773add4-fc58-4ae1-b365-244fa2a97ed2",
                    "LayerId": "b6352b42-b38c-4a66-9cb4-6d3000ff0d8a"
                },
                {
                    "id": "b9ac0058-15cf-40d1-b8dc-b900c2173429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3773add4-fc58-4ae1-b365-244fa2a97ed2",
                    "LayerId": "749bb517-b4fd-4d51-aa9c-303c5b965e1f"
                }
            ]
        },
        {
            "id": "a5f41a99-97e5-4830-b9cb-86aab4bdbf0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b890a238-3845-46f4-b983-ba9179eb5e63",
            "compositeImage": {
                "id": "57abd543-a2c6-4cdc-b56d-699a6e90d844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5f41a99-97e5-4830-b9cb-86aab4bdbf0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e0ebcb-151e-489e-9d3f-49e8db3cdaa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f41a99-97e5-4830-b9cb-86aab4bdbf0f",
                    "LayerId": "df54bd66-9a6d-4982-b630-44e9c34fd6d5"
                },
                {
                    "id": "aceb8c27-27cd-4b09-b1d4-83752293efa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f41a99-97e5-4830-b9cb-86aab4bdbf0f",
                    "LayerId": "b6352b42-b38c-4a66-9cb4-6d3000ff0d8a"
                },
                {
                    "id": "2909d25a-8568-4a42-a93a-a2ba90b4dd4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5f41a99-97e5-4830-b9cb-86aab4bdbf0f",
                    "LayerId": "749bb517-b4fd-4d51-aa9c-303c5b965e1f"
                }
            ]
        },
        {
            "id": "c21be1a9-ebad-44b1-b887-ee71ec6cb63e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b890a238-3845-46f4-b983-ba9179eb5e63",
            "compositeImage": {
                "id": "e21dedf3-bfa9-496c-ac17-1c0b81f410ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c21be1a9-ebad-44b1-b887-ee71ec6cb63e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c50c6e4-70ec-4bb0-9a08-8d9fcd2b1cd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c21be1a9-ebad-44b1-b887-ee71ec6cb63e",
                    "LayerId": "df54bd66-9a6d-4982-b630-44e9c34fd6d5"
                },
                {
                    "id": "3dcfcd72-3ab6-4cbd-885f-fb310c50f200",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c21be1a9-ebad-44b1-b887-ee71ec6cb63e",
                    "LayerId": "b6352b42-b38c-4a66-9cb4-6d3000ff0d8a"
                },
                {
                    "id": "28e51025-d1a5-4702-a46b-e4daec5f8e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c21be1a9-ebad-44b1-b887-ee71ec6cb63e",
                    "LayerId": "749bb517-b4fd-4d51-aa9c-303c5b965e1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "df54bd66-9a6d-4982-b630-44e9c34fd6d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b890a238-3845-46f4-b983-ba9179eb5e63",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b6352b42-b38c-4a66-9cb4-6d3000ff0d8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b890a238-3845-46f4-b983-ba9179eb5e63",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "749bb517-b4fd-4d51-aa9c-303c5b965e1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b890a238-3845-46f4-b983-ba9179eb5e63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 52,
    "yorig": 20
}