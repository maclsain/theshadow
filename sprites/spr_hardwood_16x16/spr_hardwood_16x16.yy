{
    "id": "b0d640e7-8e07-4e94-b341-a09affe0830f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hardwood_16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "74ab5329-507e-4f2d-8afa-95bc1c9cf956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d640e7-8e07-4e94-b341-a09affe0830f",
            "compositeImage": {
                "id": "f748bde2-0cf6-419a-9125-a57ddb282954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74ab5329-507e-4f2d-8afa-95bc1c9cf956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e150354-81be-41ab-9cb2-6655d174a2ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74ab5329-507e-4f2d-8afa-95bc1c9cf956",
                    "LayerId": "b35b15da-e2d0-4ee6-9716-1341d8c17614"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b35b15da-e2d0-4ee6-9716-1341d8c17614",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0d640e7-8e07-4e94-b341-a09affe0830f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 45,
    "yorig": -37
}