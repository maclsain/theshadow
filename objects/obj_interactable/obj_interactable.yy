{
    "id": "56a229ff-8f87-4d37-852a-a7e4e45044ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable",
    "eventList": [
        {
            "id": "8f2ab630-17fd-451b-b0a7-11173290e1d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "56a229ff-8f87-4d37-852a-a7e4e45044ed"
        },
        {
            "id": "d275ec0a-8397-417c-b7fa-2f468411dd51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "56a229ff-8f87-4d37-852a-a7e4e45044ed"
        },
        {
            "id": "204036c1-8579-46c3-8d79-dc0256978cd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "56a229ff-8f87-4d37-852a-a7e4e45044ed"
        },
        {
            "id": "a71adeed-5743-44fb-b268-e17695ed52bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "56a229ff-8f87-4d37-852a-a7e4e45044ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}