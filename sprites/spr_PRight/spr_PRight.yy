{
    "id": "ad8768e1-e594-464a-a199-aabb85ac0f45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18dc5b0c-1709-4305-bcd6-89c43bff2b04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad8768e1-e594-464a-a199-aabb85ac0f45",
            "compositeImage": {
                "id": "399bc508-df45-4334-a68c-76db0b0c445a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18dc5b0c-1709-4305-bcd6-89c43bff2b04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a4a52b3-9be1-49b4-9734-c3ea09dafa4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18dc5b0c-1709-4305-bcd6-89c43bff2b04",
                    "LayerId": "e6f34b1b-11ae-440c-9760-d5977f392e87"
                },
                {
                    "id": "c3537666-72c4-4393-b732-e1b921113b1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18dc5b0c-1709-4305-bcd6-89c43bff2b04",
                    "LayerId": "a2e7e48b-726c-4bd4-a854-5f96787e7379"
                },
                {
                    "id": "c1e094e7-3d01-4cb4-b5a8-911c171545c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18dc5b0c-1709-4305-bcd6-89c43bff2b04",
                    "LayerId": "8d0e2412-32b6-45b5-a5bf-2ca7a3293ba7"
                }
            ]
        },
        {
            "id": "6fcd8abf-e6dd-4f3e-8ded-649317df685e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad8768e1-e594-464a-a199-aabb85ac0f45",
            "compositeImage": {
                "id": "3fd0472e-99d1-45d6-b63e-6d78e642cc82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fcd8abf-e6dd-4f3e-8ded-649317df685e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "710ec48a-ac9f-45c2-b079-2c26ae11bbda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fcd8abf-e6dd-4f3e-8ded-649317df685e",
                    "LayerId": "e6f34b1b-11ae-440c-9760-d5977f392e87"
                },
                {
                    "id": "5f85b2c5-1b07-4868-ba48-b8376973510c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fcd8abf-e6dd-4f3e-8ded-649317df685e",
                    "LayerId": "a2e7e48b-726c-4bd4-a854-5f96787e7379"
                },
                {
                    "id": "8e3194b1-1753-456b-9807-ec4264372b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fcd8abf-e6dd-4f3e-8ded-649317df685e",
                    "LayerId": "8d0e2412-32b6-45b5-a5bf-2ca7a3293ba7"
                }
            ]
        },
        {
            "id": "f5360820-d026-42b2-bb65-d0a39477d2d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad8768e1-e594-464a-a199-aabb85ac0f45",
            "compositeImage": {
                "id": "0beae3bb-11f5-4b7f-825b-480ce2a93fad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5360820-d026-42b2-bb65-d0a39477d2d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "935da8ee-341f-4b0c-a731-289e3aac5311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5360820-d026-42b2-bb65-d0a39477d2d0",
                    "LayerId": "e6f34b1b-11ae-440c-9760-d5977f392e87"
                },
                {
                    "id": "2bdbdc1a-e66d-4cb8-be0b-26098cd7528d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5360820-d026-42b2-bb65-d0a39477d2d0",
                    "LayerId": "a2e7e48b-726c-4bd4-a854-5f96787e7379"
                },
                {
                    "id": "8099452e-f7d1-4ae8-943c-f1118a96cfa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5360820-d026-42b2-bb65-d0a39477d2d0",
                    "LayerId": "8d0e2412-32b6-45b5-a5bf-2ca7a3293ba7"
                }
            ]
        },
        {
            "id": "221bd558-8fe7-4cc7-807a-9aea7462efc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad8768e1-e594-464a-a199-aabb85ac0f45",
            "compositeImage": {
                "id": "16aaac95-5dd0-42c4-a2ff-9529518a1dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "221bd558-8fe7-4cc7-807a-9aea7462efc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1abb0a40-887c-4336-9bb1-72e24640d9db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221bd558-8fe7-4cc7-807a-9aea7462efc6",
                    "LayerId": "e6f34b1b-11ae-440c-9760-d5977f392e87"
                },
                {
                    "id": "87385ef9-0b95-4f11-9c99-c6357fe1a734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221bd558-8fe7-4cc7-807a-9aea7462efc6",
                    "LayerId": "a2e7e48b-726c-4bd4-a854-5f96787e7379"
                },
                {
                    "id": "1ac0ca22-119f-4def-96a1-fab8d5c1caab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221bd558-8fe7-4cc7-807a-9aea7462efc6",
                    "LayerId": "8d0e2412-32b6-45b5-a5bf-2ca7a3293ba7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e6f34b1b-11ae-440c-9760-d5977f392e87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad8768e1-e594-464a-a199-aabb85ac0f45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a2e7e48b-726c-4bd4-a854-5f96787e7379",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad8768e1-e594-464a-a199-aabb85ac0f45",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8d0e2412-32b6-45b5-a5bf-2ca7a3293ba7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad8768e1-e594-464a-a199-aabb85ac0f45",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 2",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}