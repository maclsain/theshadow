{
    "id": "f2646334-fbdb-409d-b9ea-228e3877ce62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dumbbell2_64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0955e86-72f4-459d-9608-6f4abde9c717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2646334-fbdb-409d-b9ea-228e3877ce62",
            "compositeImage": {
                "id": "13b1822c-839f-472a-804b-0e14aea4fc28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0955e86-72f4-459d-9608-6f4abde9c717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c23a5c3-b974-4e13-bfb2-4e0ca8334157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0955e86-72f4-459d-9608-6f4abde9c717",
                    "LayerId": "e488b089-2d16-42b4-9e82-f405983c72cc"
                },
                {
                    "id": "d1f9ab7a-8969-480b-bf6e-692c5bd6a7a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0955e86-72f4-459d-9608-6f4abde9c717",
                    "LayerId": "ecc8b1bb-8108-4ab5-b236-8ce779066b6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e488b089-2d16-42b4-9e82-f405983c72cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2646334-fbdb-409d-b9ea-228e3877ce62",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ecc8b1bb-8108-4ab5-b236-8ce779066b6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2646334-fbdb-409d-b9ea-228e3877ce62",
            "blendMode": 0,
            "isLocked": false,
            "name": "bg",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}