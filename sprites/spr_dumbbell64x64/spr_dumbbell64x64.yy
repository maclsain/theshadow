{
    "id": "8873600b-2433-47ac-ae68-0e423f9fd107",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dumbbell64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73fa8793-3935-4093-95ae-4b94ff89dc4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8873600b-2433-47ac-ae68-0e423f9fd107",
            "compositeImage": {
                "id": "5257eb9d-9cee-43c4-a956-ef78e4ddae2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73fa8793-3935-4093-95ae-4b94ff89dc4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6987a605-613c-4e3a-86a8-3aff64e2c5d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73fa8793-3935-4093-95ae-4b94ff89dc4b",
                    "LayerId": "e1aeb91f-91bf-4ffa-96b9-e8c34b5fbab4"
                },
                {
                    "id": "851b5eab-a66c-4aeb-a862-3e39e7597197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73fa8793-3935-4093-95ae-4b94ff89dc4b",
                    "LayerId": "89101083-553b-42fb-8af1-220f15ebc880"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e1aeb91f-91bf-4ffa-96b9-e8c34b5fbab4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8873600b-2433-47ac-ae68-0e423f9fd107",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "89101083-553b-42fb-8af1-220f15ebc880",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8873600b-2433-47ac-ae68-0e423f9fd107",
            "blendMode": 0,
            "isLocked": false,
            "name": "bg",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}