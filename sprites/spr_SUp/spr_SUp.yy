{
    "id": "c3102631-ce28-4761-ab50-c26893c24b24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07a1ca52-a48e-4e18-b5c0-f000778ed61e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3102631-ce28-4761-ab50-c26893c24b24",
            "compositeImage": {
                "id": "83748cd3-9bc6-4632-932e-0fde531f4f0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07a1ca52-a48e-4e18-b5c0-f000778ed61e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ac23a1-78ad-4a29-909b-7738f54a54b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a1ca52-a48e-4e18-b5c0-f000778ed61e",
                    "LayerId": "aafd8fe2-c3c6-4131-8d6b-6378b5574e1d"
                },
                {
                    "id": "dff8b4bf-c36d-447f-a29a-77b16ca23e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a1ca52-a48e-4e18-b5c0-f000778ed61e",
                    "LayerId": "74210bc9-bbc7-440f-89ec-5a751cc651d1"
                },
                {
                    "id": "d8742ccb-efc1-4188-bc41-1f3ef41ac802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a1ca52-a48e-4e18-b5c0-f000778ed61e",
                    "LayerId": "4c7ed7b6-cc8b-4610-867d-be67fd5ec868"
                }
            ]
        },
        {
            "id": "76ccd598-c5ce-4dc9-a57d-c16779744f60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3102631-ce28-4761-ab50-c26893c24b24",
            "compositeImage": {
                "id": "166d495a-e389-4fd6-93b4-ffe6efd7b0fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ccd598-c5ce-4dc9-a57d-c16779744f60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "915754a3-a0c7-4fc5-baf6-407db67c5edb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ccd598-c5ce-4dc9-a57d-c16779744f60",
                    "LayerId": "aafd8fe2-c3c6-4131-8d6b-6378b5574e1d"
                },
                {
                    "id": "653633d9-6f10-4adf-aeac-7d7719073369",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ccd598-c5ce-4dc9-a57d-c16779744f60",
                    "LayerId": "74210bc9-bbc7-440f-89ec-5a751cc651d1"
                },
                {
                    "id": "b9021824-a45a-4319-8e2f-d56f47b82554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ccd598-c5ce-4dc9-a57d-c16779744f60",
                    "LayerId": "4c7ed7b6-cc8b-4610-867d-be67fd5ec868"
                }
            ]
        },
        {
            "id": "1fcebc1e-6384-41e9-bf29-f2bdadf5b178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3102631-ce28-4761-ab50-c26893c24b24",
            "compositeImage": {
                "id": "37416177-a3d5-4d43-8ce9-76f92c874f78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fcebc1e-6384-41e9-bf29-f2bdadf5b178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e141f9f-9ee8-45c0-aab5-7482193288b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fcebc1e-6384-41e9-bf29-f2bdadf5b178",
                    "LayerId": "aafd8fe2-c3c6-4131-8d6b-6378b5574e1d"
                },
                {
                    "id": "c84b92f0-5923-4a68-bf68-54052d14c836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fcebc1e-6384-41e9-bf29-f2bdadf5b178",
                    "LayerId": "74210bc9-bbc7-440f-89ec-5a751cc651d1"
                },
                {
                    "id": "e921fc42-39cd-4f90-8b19-7e8b5bc03ecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fcebc1e-6384-41e9-bf29-f2bdadf5b178",
                    "LayerId": "4c7ed7b6-cc8b-4610-867d-be67fd5ec868"
                }
            ]
        },
        {
            "id": "cb72aa37-9d2a-4ef9-825c-24945695f8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3102631-ce28-4761-ab50-c26893c24b24",
            "compositeImage": {
                "id": "e2434b5a-cb2b-40e1-b581-f14ea4116fcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb72aa37-9d2a-4ef9-825c-24945695f8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14cb487b-e1f5-4037-9397-102a2af913db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb72aa37-9d2a-4ef9-825c-24945695f8d4",
                    "LayerId": "aafd8fe2-c3c6-4131-8d6b-6378b5574e1d"
                },
                {
                    "id": "a71dc70c-131d-409c-896d-57ab326881f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb72aa37-9d2a-4ef9-825c-24945695f8d4",
                    "LayerId": "74210bc9-bbc7-440f-89ec-5a751cc651d1"
                },
                {
                    "id": "6d542e07-2e62-4c5d-9ca8-543aac3ecb27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb72aa37-9d2a-4ef9-825c-24945695f8d4",
                    "LayerId": "4c7ed7b6-cc8b-4610-867d-be67fd5ec868"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aafd8fe2-c3c6-4131-8d6b-6378b5574e1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3102631-ce28-4761-ab50-c26893c24b24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "74210bc9-bbc7-440f-89ec-5a751cc651d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3102631-ce28-4761-ab50-c26893c24b24",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "4c7ed7b6-cc8b-4610-867d-be67fd5ec868",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3102631-ce28-4761-ab50-c26893c24b24",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 2",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}