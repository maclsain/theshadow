{
    "id": "65b62beb-6d80-4193-9a85-4cc9daa01a8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PSweat8x8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1da31f4-3a24-40cd-89b7-3d8e1da26291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65b62beb-6d80-4193-9a85-4cc9daa01a8c",
            "compositeImage": {
                "id": "aea148ae-6e2f-4c58-88c2-cad28ed45c8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1da31f4-3a24-40cd-89b7-3d8e1da26291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6a160d0-b5fc-4127-83b4-ec8afe41bdbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1da31f4-3a24-40cd-89b7-3d8e1da26291",
                    "LayerId": "3c6e737d-31d2-428b-8a58-7bed5d2bcc41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "3c6e737d-31d2-428b-8a58-7bed5d2bcc41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65b62beb-6d80-4193-9a85-4cc9daa01a8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 7
}