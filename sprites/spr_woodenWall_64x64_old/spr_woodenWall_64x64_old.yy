{
    "id": "d3ee64a2-623d-4ba1-b783-5c6584d050c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_woodenWall_64x64_old",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bcd284d-91b4-4a8f-9833-e597906e9515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3ee64a2-623d-4ba1-b783-5c6584d050c5",
            "compositeImage": {
                "id": "97c345f9-2d02-4054-b474-7a8599506fce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bcd284d-91b4-4a8f-9833-e597906e9515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b278a1b2-d553-4430-9b0a-ac3f861f50a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bcd284d-91b4-4a8f-9833-e597906e9515",
                    "LayerId": "8ee2d417-b180-40f1-a8bb-4c4534501fcc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8ee2d417-b180-40f1-a8bb-4c4534501fcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3ee64a2-623d-4ba1-b783-5c6584d050c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}