{
    "id": "1682b3c1-6868-4c90-a39a-8a06647152aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anim_PLeft_16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2da8974b-a75f-4dd4-9ee9-980f35545254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1682b3c1-6868-4c90-a39a-8a06647152aa",
            "compositeImage": {
                "id": "b7b91c3c-39f4-4113-81de-7015f4e20e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2da8974b-a75f-4dd4-9ee9-980f35545254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ed7f7b8-fcb9-4395-9779-21b47ebe520d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2da8974b-a75f-4dd4-9ee9-980f35545254",
                    "LayerId": "86438639-e85e-4cd0-8ba0-728e2e1fb968"
                }
            ]
        },
        {
            "id": "056a8cef-832d-4084-8557-77b81e491af9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1682b3c1-6868-4c90-a39a-8a06647152aa",
            "compositeImage": {
                "id": "62d3b999-7750-419a-ac69-362cb0a6a6ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "056a8cef-832d-4084-8557-77b81e491af9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08796b90-92e8-4312-82f9-3ae52922d69f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "056a8cef-832d-4084-8557-77b81e491af9",
                    "LayerId": "86438639-e85e-4cd0-8ba0-728e2e1fb968"
                }
            ]
        },
        {
            "id": "f05529b2-3258-4c7d-9e8d-4fb85d456b8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1682b3c1-6868-4c90-a39a-8a06647152aa",
            "compositeImage": {
                "id": "27597a64-07e4-4f81-a678-b9764c20caaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f05529b2-3258-4c7d-9e8d-4fb85d456b8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7352317e-58bb-47e6-a782-d23c66e71949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f05529b2-3258-4c7d-9e8d-4fb85d456b8e",
                    "LayerId": "86438639-e85e-4cd0-8ba0-728e2e1fb968"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "86438639-e85e-4cd0-8ba0-728e2e1fb968",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1682b3c1-6868-4c90-a39a-8a06647152aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}