{
    "id": "c855fadd-ba26-4cdd-8b1b-0bd40e1417c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collider64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a0d4ff4-d256-431a-a18f-0cfb95643ffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c855fadd-ba26-4cdd-8b1b-0bd40e1417c8",
            "compositeImage": {
                "id": "b73c6c68-8e06-42bc-ad2e-00dcdbb6e311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a0d4ff4-d256-431a-a18f-0cfb95643ffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "183db463-5515-430a-84fd-d278bdfe7a93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a0d4ff4-d256-431a-a18f-0cfb95643ffa",
                    "LayerId": "ac920883-f789-47b8-865c-6d21e7060ee5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ac920883-f789-47b8-865c-6d21e7060ee5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c855fadd-ba26-4cdd-8b1b-0bd40e1417c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}