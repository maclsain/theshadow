{
    "id": "28d76002-8d61-476a-9529-c1f2e0b9f7ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Exercise",
    "eventList": [
        {
            "id": "08210396-84aa-4941-9a50-745b7ad61990",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "72684c12-9592-47fa-8c72-90dbfa3bcceb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "28d76002-8d61-476a-9529-c1f2e0b9f7ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "56a229ff-8f87-4d37-852a-a7e4e45044ed",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "f2646334-fbdb-409d-b9ea-228e3877ce62",
    "visible": true
}