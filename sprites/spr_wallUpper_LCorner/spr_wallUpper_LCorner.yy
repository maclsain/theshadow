{
    "id": "3c72271e-256d-4ca8-8815-ea9e61dfda1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wallUpper_LCorner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc704a21-6d49-4132-aa31-a50ec711e28d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c72271e-256d-4ca8-8815-ea9e61dfda1e",
            "compositeImage": {
                "id": "bf52cb9f-06d3-4792-b060-1554300bd716",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc704a21-6d49-4132-aa31-a50ec711e28d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bfe333c-3d71-45c7-a657-cd00db5722d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc704a21-6d49-4132-aa31-a50ec711e28d",
                    "LayerId": "b2e1eaaf-06e3-4ff1-816c-8778a510d7f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b2e1eaaf-06e3-4ff1-816c-8778a510d7f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c72271e-256d-4ca8-8815-ea9e61dfda1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}