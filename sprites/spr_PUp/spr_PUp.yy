{
    "id": "346df8f2-a4d7-4923-ad3c-dff4e364404b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "588a1423-491a-470b-86f7-d556f30d1abf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "346df8f2-a4d7-4923-ad3c-dff4e364404b",
            "compositeImage": {
                "id": "7204cf21-8a8c-4b86-ae75-450af437f4f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "588a1423-491a-470b-86f7-d556f30d1abf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "740644e3-b7ee-4dfc-8ae4-2286ac21eb8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "588a1423-491a-470b-86f7-d556f30d1abf",
                    "LayerId": "288564fa-dd62-44c8-b4df-cb535eca76ce"
                },
                {
                    "id": "4ed0050c-ac24-4ca2-a953-8176c8330296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "588a1423-491a-470b-86f7-d556f30d1abf",
                    "LayerId": "9ea3f9cc-f2d1-4eed-a2cd-308e1dbf4cbb"
                },
                {
                    "id": "d7fd82ef-568b-4378-97bd-20ff320514fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "588a1423-491a-470b-86f7-d556f30d1abf",
                    "LayerId": "0eb53862-6103-443a-80d9-e5acae100f8d"
                }
            ]
        },
        {
            "id": "c2c2eccb-8ccc-42b8-812f-803d8d93538a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "346df8f2-a4d7-4923-ad3c-dff4e364404b",
            "compositeImage": {
                "id": "2cfa935a-3cac-4c70-ac18-819fcd8a51b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c2eccb-8ccc-42b8-812f-803d8d93538a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b299ec7-33f4-4b6c-934b-c191689e44a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c2eccb-8ccc-42b8-812f-803d8d93538a",
                    "LayerId": "288564fa-dd62-44c8-b4df-cb535eca76ce"
                },
                {
                    "id": "769d2fe9-de78-4350-8bc3-8586ad69cda5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c2eccb-8ccc-42b8-812f-803d8d93538a",
                    "LayerId": "9ea3f9cc-f2d1-4eed-a2cd-308e1dbf4cbb"
                },
                {
                    "id": "8c4ab57c-ad90-4b37-9939-d88968a7c18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c2eccb-8ccc-42b8-812f-803d8d93538a",
                    "LayerId": "0eb53862-6103-443a-80d9-e5acae100f8d"
                }
            ]
        },
        {
            "id": "3cb44cdb-5269-418d-b3c4-d00ff5069ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "346df8f2-a4d7-4923-ad3c-dff4e364404b",
            "compositeImage": {
                "id": "199ab530-c979-4af6-8743-c0c58d18ad21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cb44cdb-5269-418d-b3c4-d00ff5069ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d8d1d53-b65c-42be-baf3-5f917cbdb44c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cb44cdb-5269-418d-b3c4-d00ff5069ee7",
                    "LayerId": "288564fa-dd62-44c8-b4df-cb535eca76ce"
                },
                {
                    "id": "746a10d6-8880-4861-b209-22933212a400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cb44cdb-5269-418d-b3c4-d00ff5069ee7",
                    "LayerId": "9ea3f9cc-f2d1-4eed-a2cd-308e1dbf4cbb"
                },
                {
                    "id": "2d36d366-9374-4554-8482-15d681c58e6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cb44cdb-5269-418d-b3c4-d00ff5069ee7",
                    "LayerId": "0eb53862-6103-443a-80d9-e5acae100f8d"
                }
            ]
        },
        {
            "id": "b25bd9b7-a961-4977-80a1-d1b9e7839ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "346df8f2-a4d7-4923-ad3c-dff4e364404b",
            "compositeImage": {
                "id": "cb28f266-bf8f-4f76-a193-d5e3fcccdf38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b25bd9b7-a961-4977-80a1-d1b9e7839ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ab1d17f-96e3-4039-bb06-362261c358d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25bd9b7-a961-4977-80a1-d1b9e7839ed1",
                    "LayerId": "288564fa-dd62-44c8-b4df-cb535eca76ce"
                },
                {
                    "id": "fa893e2c-af5c-4f0d-9613-3d7f51025a0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25bd9b7-a961-4977-80a1-d1b9e7839ed1",
                    "LayerId": "9ea3f9cc-f2d1-4eed-a2cd-308e1dbf4cbb"
                },
                {
                    "id": "1cd362f1-daac-4168-88cd-ad9c711e9fd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25bd9b7-a961-4977-80a1-d1b9e7839ed1",
                    "LayerId": "0eb53862-6103-443a-80d9-e5acae100f8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "288564fa-dd62-44c8-b4df-cb535eca76ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "346df8f2-a4d7-4923-ad3c-dff4e364404b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9ea3f9cc-f2d1-4eed-a2cd-308e1dbf4cbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "346df8f2-a4d7-4923-ad3c-dff4e364404b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0eb53862-6103-443a-80d9-e5acae100f8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "346df8f2-a4d7-4923-ad3c-dff4e364404b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 2",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}