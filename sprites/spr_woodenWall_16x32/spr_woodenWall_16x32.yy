{
    "id": "0fa01e40-2259-459d-83cf-bf91879d2e19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_woodenWall_16x32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f2b1c35-b7f2-44d5-b6af-c9ef14c4a279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fa01e40-2259-459d-83cf-bf91879d2e19",
            "compositeImage": {
                "id": "8de5f91b-035c-4c04-a33c-e70c466c0f9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f2b1c35-b7f2-44d5-b6af-c9ef14c4a279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e4cda7f-6e68-4450-a22a-9b4bfb4225d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f2b1c35-b7f2-44d5-b6af-c9ef14c4a279",
                    "LayerId": "84016faa-94d4-47aa-b476-6e600ec9762d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "84016faa-94d4-47aa-b476-6e600ec9762d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fa01e40-2259-459d-83cf-bf91879d2e19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}