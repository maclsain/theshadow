{
    "id": "18def417-3ec4-4789-90b8-a630e660544c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Pokemon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 284,
    "bbox_left": 0,
    "bbox_right": 259,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c44fed3f-4bcf-47ca-b2d3-80f07472e551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18def417-3ec4-4789-90b8-a630e660544c",
            "compositeImage": {
                "id": "5fc1f534-eb70-49cc-a122-5173a36186ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c44fed3f-4bcf-47ca-b2d3-80f07472e551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da98ee04-bf69-44f0-9d43-0961a85e436d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c44fed3f-4bcf-47ca-b2d3-80f07472e551",
                    "LayerId": "f10b0b0f-b1fa-4759-a3c8-baf60bf278c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 285,
    "layers": [
        {
            "id": "f10b0b0f-b1fa-4759-a3c8-baf60bf278c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18def417-3ec4-4789-90b8-a630e660544c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 260,
    "xorig": 0,
    "yorig": 0
}