{
    "id": "db0a1da9-8c0e-48ea-b8d0-97af0d542168",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PWeight16x16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "021e4180-c019-4e15-a9ff-23657fc1a448",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0a1da9-8c0e-48ea-b8d0-97af0d542168",
            "compositeImage": {
                "id": "9b6468d9-44d4-47ae-a574-47dd4869e740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "021e4180-c019-4e15-a9ff-23657fc1a448",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b465cfbf-0b1b-443a-a294-3eef16385750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021e4180-c019-4e15-a9ff-23657fc1a448",
                    "LayerId": "6979e8a7-5229-4dd5-a491-9ac8e5a2bd9d"
                },
                {
                    "id": "b1025381-d188-4983-bef3-106cac781723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021e4180-c019-4e15-a9ff-23657fc1a448",
                    "LayerId": "241e1fb5-251d-47a4-af98-657a24872f76"
                }
            ]
        },
        {
            "id": "be1457ff-5f8d-4f5c-b5f8-c512439fe2d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0a1da9-8c0e-48ea-b8d0-97af0d542168",
            "compositeImage": {
                "id": "ce9f5c26-9ff6-4191-93a1-d7f12f720ab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1457ff-5f8d-4f5c-b5f8-c512439fe2d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bf6b970-d214-4196-ab3b-476622af738f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1457ff-5f8d-4f5c-b5f8-c512439fe2d3",
                    "LayerId": "6979e8a7-5229-4dd5-a491-9ac8e5a2bd9d"
                },
                {
                    "id": "9db6cc37-fdf1-46e9-bdcd-7a86837ae46e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1457ff-5f8d-4f5c-b5f8-c512439fe2d3",
                    "LayerId": "241e1fb5-251d-47a4-af98-657a24872f76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6979e8a7-5229-4dd5-a491-9ac8e5a2bd9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db0a1da9-8c0e-48ea-b8d0-97af0d542168",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "241e1fb5-251d-47a4-af98-657a24872f76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db0a1da9-8c0e-48ea-b8d0-97af0d542168",
            "blendMode": 0,
            "isLocked": false,
            "name": "bg",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}