{
    "id": "08320e75-e971-464e-b2e0-af3949d382f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a32d996-558f-4fd1-b397-819d30160b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08320e75-e971-464e-b2e0-af3949d382f2",
            "compositeImage": {
                "id": "407d0dd4-bbb1-451b-81eb-cc24979ef23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a32d996-558f-4fd1-b397-819d30160b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e907e927-0a6e-4407-b1f8-76bd8d11ef86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a32d996-558f-4fd1-b397-819d30160b99",
                    "LayerId": "bc319741-8814-4e7b-bf4d-e3cf8f56772b"
                },
                {
                    "id": "0afc2325-fb33-40eb-92f7-e9558da113ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a32d996-558f-4fd1-b397-819d30160b99",
                    "LayerId": "ca0032f0-c220-4993-8350-27840383fd61"
                },
                {
                    "id": "65d9485a-b5dc-4fe4-a4e4-0666a281bd69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a32d996-558f-4fd1-b397-819d30160b99",
                    "LayerId": "59848c4d-494f-47f0-95f7-f7a7f51a6604"
                }
            ]
        },
        {
            "id": "189b5632-9c14-4cf9-996e-f91c0386304b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08320e75-e971-464e-b2e0-af3949d382f2",
            "compositeImage": {
                "id": "090e8257-6741-4516-9e66-bb2f3ccadd61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "189b5632-9c14-4cf9-996e-f91c0386304b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d32303f-e822-4e79-b013-30a803b52c7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189b5632-9c14-4cf9-996e-f91c0386304b",
                    "LayerId": "bc319741-8814-4e7b-bf4d-e3cf8f56772b"
                },
                {
                    "id": "064e4ede-74fb-41fd-9454-869f003fd4d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189b5632-9c14-4cf9-996e-f91c0386304b",
                    "LayerId": "ca0032f0-c220-4993-8350-27840383fd61"
                },
                {
                    "id": "a5b648a8-3b37-41a4-92c0-a0bb466d3b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189b5632-9c14-4cf9-996e-f91c0386304b",
                    "LayerId": "59848c4d-494f-47f0-95f7-f7a7f51a6604"
                }
            ]
        },
        {
            "id": "1f32ed63-22cb-4652-b7aa-145bb316ce94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08320e75-e971-464e-b2e0-af3949d382f2",
            "compositeImage": {
                "id": "f41da4c5-fb60-4a3d-b7a7-82f54f9c681e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f32ed63-22cb-4652-b7aa-145bb316ce94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71b9d1f8-10ec-411d-9826-b3712e5a7bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f32ed63-22cb-4652-b7aa-145bb316ce94",
                    "LayerId": "bc319741-8814-4e7b-bf4d-e3cf8f56772b"
                },
                {
                    "id": "92580fae-7540-4c53-9815-0f65d46a1be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f32ed63-22cb-4652-b7aa-145bb316ce94",
                    "LayerId": "ca0032f0-c220-4993-8350-27840383fd61"
                },
                {
                    "id": "3924ae4f-c730-4814-908f-09f4301ba0a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f32ed63-22cb-4652-b7aa-145bb316ce94",
                    "LayerId": "59848c4d-494f-47f0-95f7-f7a7f51a6604"
                }
            ]
        },
        {
            "id": "8f400898-884d-4a26-b541-e043b43feff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08320e75-e971-464e-b2e0-af3949d382f2",
            "compositeImage": {
                "id": "3e77582c-1e6b-4a59-8aa2-998843f20171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f400898-884d-4a26-b541-e043b43feff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22be9296-7617-4f88-9da2-c8258aa16749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f400898-884d-4a26-b541-e043b43feff2",
                    "LayerId": "bc319741-8814-4e7b-bf4d-e3cf8f56772b"
                },
                {
                    "id": "e582a827-e37f-45f3-b636-4708bdaf4bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f400898-884d-4a26-b541-e043b43feff2",
                    "LayerId": "ca0032f0-c220-4993-8350-27840383fd61"
                },
                {
                    "id": "2cdf8d78-3962-4bc8-a4be-e4a1edfa5156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f400898-884d-4a26-b541-e043b43feff2",
                    "LayerId": "59848c4d-494f-47f0-95f7-f7a7f51a6604"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bc319741-8814-4e7b-bf4d-e3cf8f56772b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08320e75-e971-464e-b2e0-af3949d382f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ca0032f0-c220-4993-8350-27840383fd61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08320e75-e971-464e-b2e0-af3949d382f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "59848c4d-494f-47f0-95f7-f7a7f51a6604",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08320e75-e971-464e-b2e0-af3949d382f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 2",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}