{
    "id": "9fafcc2f-1182-4cdc-955d-107d7b8b9c8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PWeight64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "766caa08-cc10-475a-bb24-840913548680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fafcc2f-1182-4cdc-955d-107d7b8b9c8d",
            "compositeImage": {
                "id": "35f7800b-f95a-4c70-bfbb-74c1951ed549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "766caa08-cc10-475a-bb24-840913548680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bafd5d0-b069-477b-8fda-5ee58cb7b876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766caa08-cc10-475a-bb24-840913548680",
                    "LayerId": "4169ccbc-6a2b-4c36-b640-f97c92b2e8d9"
                },
                {
                    "id": "7f6fb328-5ac1-4ef8-9624-761e3e61f9ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766caa08-cc10-475a-bb24-840913548680",
                    "LayerId": "2a0e581b-eca1-4719-a637-e45526e5393e"
                }
            ]
        },
        {
            "id": "8aecad99-8798-42fb-9543-12c3f7228e80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fafcc2f-1182-4cdc-955d-107d7b8b9c8d",
            "compositeImage": {
                "id": "d2269583-7c93-4290-bfdb-0b2e05ac6645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aecad99-8798-42fb-9543-12c3f7228e80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f231dd20-c7e4-4b96-ba8e-6043a4918c5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aecad99-8798-42fb-9543-12c3f7228e80",
                    "LayerId": "4169ccbc-6a2b-4c36-b640-f97c92b2e8d9"
                },
                {
                    "id": "b4732564-ab2f-42cb-8be2-9068ce7d5fb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aecad99-8798-42fb-9543-12c3f7228e80",
                    "LayerId": "2a0e581b-eca1-4719-a637-e45526e5393e"
                }
            ]
        },
        {
            "id": "e9ba3e42-505e-476c-97d3-48f32a8e0c8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fafcc2f-1182-4cdc-955d-107d7b8b9c8d",
            "compositeImage": {
                "id": "bac287e4-5186-4bee-823a-5c0d09343cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ba3e42-505e-476c-97d3-48f32a8e0c8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb56a6e-66b3-4574-aac0-050964fd69aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ba3e42-505e-476c-97d3-48f32a8e0c8b",
                    "LayerId": "4169ccbc-6a2b-4c36-b640-f97c92b2e8d9"
                },
                {
                    "id": "a1932ed7-16d6-4304-9b35-ad2b82e6b97a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ba3e42-505e-476c-97d3-48f32a8e0c8b",
                    "LayerId": "2a0e581b-eca1-4719-a637-e45526e5393e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4169ccbc-6a2b-4c36-b640-f97c92b2e8d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fafcc2f-1182-4cdc-955d-107d7b8b9c8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2a0e581b-eca1-4719-a637-e45526e5393e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fafcc2f-1182-4cdc-955d-107d7b8b9c8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "bg",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}