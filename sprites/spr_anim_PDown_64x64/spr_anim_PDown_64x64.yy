{
    "id": "da04b56a-489d-4c4d-937d-c18e0b795e9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_anim_PDown_64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1face3c7-3317-413e-ad05-45865a0bf15b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da04b56a-489d-4c4d-937d-c18e0b795e9a",
            "compositeImage": {
                "id": "fd899af4-5448-4101-a155-3a02dea8aebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1face3c7-3317-413e-ad05-45865a0bf15b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98b3898b-ef9b-499b-a748-fd12ea9b46ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1face3c7-3317-413e-ad05-45865a0bf15b",
                    "LayerId": "3f79ed85-95ac-44a8-8c85-47afbb1cdf30"
                },
                {
                    "id": "89536d68-db21-47d9-8c1e-ff80fed1ece3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1face3c7-3317-413e-ad05-45865a0bf15b",
                    "LayerId": "29b6a0c1-b74d-4ad4-82f5-b1dfb5b54cd1"
                }
            ]
        },
        {
            "id": "afe7f970-07e3-4c8f-88bf-a32e4bb66e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da04b56a-489d-4c4d-937d-c18e0b795e9a",
            "compositeImage": {
                "id": "1b4b5376-bb8a-45f0-a44f-32fc9782cc92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe7f970-07e3-4c8f-88bf-a32e4bb66e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06b5010a-26c4-4168-b5d6-731ce5cfd1c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe7f970-07e3-4c8f-88bf-a32e4bb66e67",
                    "LayerId": "3f79ed85-95ac-44a8-8c85-47afbb1cdf30"
                },
                {
                    "id": "0e7ff89e-d820-4024-999e-e083e1f40772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe7f970-07e3-4c8f-88bf-a32e4bb66e67",
                    "LayerId": "29b6a0c1-b74d-4ad4-82f5-b1dfb5b54cd1"
                }
            ]
        },
        {
            "id": "ceec318b-5de5-4c4a-a98f-9d459a41adf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da04b56a-489d-4c4d-937d-c18e0b795e9a",
            "compositeImage": {
                "id": "97b0182b-cfc3-402d-8217-8d39b6e71f71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceec318b-5de5-4c4a-a98f-9d459a41adf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b18e102-ae41-4283-bd8e-9279e292293a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceec318b-5de5-4c4a-a98f-9d459a41adf7",
                    "LayerId": "3f79ed85-95ac-44a8-8c85-47afbb1cdf30"
                },
                {
                    "id": "4bcc775b-0fa3-4ba9-a23a-930405675992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceec318b-5de5-4c4a-a98f-9d459a41adf7",
                    "LayerId": "29b6a0c1-b74d-4ad4-82f5-b1dfb5b54cd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3f79ed85-95ac-44a8-8c85-47afbb1cdf30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da04b56a-489d-4c4d-937d-c18e0b795e9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "29b6a0c1-b74d-4ad4-82f5-b1dfb5b54cd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da04b56a-489d-4c4d-937d-c18e0b795e9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}