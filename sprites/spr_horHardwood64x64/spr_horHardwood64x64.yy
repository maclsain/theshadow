{
    "id": "b34e951c-5470-4fb2-9dd1-4961e3421a5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_horHardwood64x64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1981d3f-eb66-4cb5-8fc9-b857b702636f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34e951c-5470-4fb2-9dd1-4961e3421a5c",
            "compositeImage": {
                "id": "b3c386b9-3e05-4e88-9732-b7666a438265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1981d3f-eb66-4cb5-8fc9-b857b702636f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e20493d-1efa-4de1-93db-e63fc535c097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1981d3f-eb66-4cb5-8fc9-b857b702636f",
                    "LayerId": "b0ad5b26-27eb-408c-8d84-a666b10ee6a6"
                }
            ]
        },
        {
            "id": "5409e734-d100-4aa2-9fff-262353fa2fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34e951c-5470-4fb2-9dd1-4961e3421a5c",
            "compositeImage": {
                "id": "dc365736-4f43-4b1d-a7d4-c134f9d6466a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5409e734-d100-4aa2-9fff-262353fa2fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7555cf6c-dddd-4888-8ece-10acd16a67b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5409e734-d100-4aa2-9fff-262353fa2fc9",
                    "LayerId": "b0ad5b26-27eb-408c-8d84-a666b10ee6a6"
                }
            ]
        },
        {
            "id": "f4ece4cd-2b31-45f5-a2cd-d75131bf1c29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34e951c-5470-4fb2-9dd1-4961e3421a5c",
            "compositeImage": {
                "id": "56c9cd68-72e5-481f-8f24-3fccc700a811",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4ece4cd-2b31-45f5-a2cd-d75131bf1c29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75e1a3ff-c2b8-47b2-a897-9da47192eda3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4ece4cd-2b31-45f5-a2cd-d75131bf1c29",
                    "LayerId": "b0ad5b26-27eb-408c-8d84-a666b10ee6a6"
                }
            ]
        },
        {
            "id": "012935c3-f755-4af4-ae0d-a60577c5f788",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34e951c-5470-4fb2-9dd1-4961e3421a5c",
            "compositeImage": {
                "id": "3538d336-7418-4c9e-87c3-b3c480e2de1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "012935c3-f755-4af4-ae0d-a60577c5f788",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c05d63e-48a0-43e8-8955-e654a643e67f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "012935c3-f755-4af4-ae0d-a60577c5f788",
                    "LayerId": "b0ad5b26-27eb-408c-8d84-a666b10ee6a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b0ad5b26-27eb-408c-8d84-a666b10ee6a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b34e951c-5470-4fb2-9dd1-4961e3421a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}