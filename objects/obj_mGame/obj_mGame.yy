{
    "id": "29b5b940-4e26-46dc-bb4a-0a2c9e807a23",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mGame",
    "eventList": [
        {
            "id": "b8e8bf46-64cf-4b41-9167-9df2878f77c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29b5b940-4e26-46dc-bb4a-0a2c9e807a23"
        },
        {
            "id": "dcb4fe37-b0b7-4660-af5a-dc220ae7d4e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "29b5b940-4e26-46dc-bb4a-0a2c9e807a23"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}