{
    "id": "fe001bc5-3ff5-42f6-ade6-dd40eaa098c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d5b830c-7370-443d-b429-9101179edc0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe001bc5-3ff5-42f6-ade6-dd40eaa098c7",
            "compositeImage": {
                "id": "01d080da-371d-4c93-b22d-25304c568f3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d5b830c-7370-443d-b429-9101179edc0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f265ed-fd80-4706-a046-dce218f81890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d5b830c-7370-443d-b429-9101179edc0d",
                    "LayerId": "e896b03c-d2bd-40c3-bb24-77c0c50bde08"
                },
                {
                    "id": "6307b55e-233a-4849-b9e9-eecca5b603e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d5b830c-7370-443d-b429-9101179edc0d",
                    "LayerId": "6105828a-1c98-454b-b515-0c3f216337e9"
                },
                {
                    "id": "1c7e5d12-ec26-4b9e-b136-e819a24055a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d5b830c-7370-443d-b429-9101179edc0d",
                    "LayerId": "7fbd77b9-e8c1-4831-8482-d3effbed954b"
                }
            ]
        },
        {
            "id": "b41885c7-bb18-4f21-94ee-b2e745c67fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe001bc5-3ff5-42f6-ade6-dd40eaa098c7",
            "compositeImage": {
                "id": "b731fc1a-1b3d-4b06-9638-ff401deb85a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b41885c7-bb18-4f21-94ee-b2e745c67fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71b03288-8251-487e-8b1f-a58baa3a24df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41885c7-bb18-4f21-94ee-b2e745c67fcb",
                    "LayerId": "e896b03c-d2bd-40c3-bb24-77c0c50bde08"
                },
                {
                    "id": "3b43a42a-cd1d-408b-9350-501e0f676429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41885c7-bb18-4f21-94ee-b2e745c67fcb",
                    "LayerId": "6105828a-1c98-454b-b515-0c3f216337e9"
                },
                {
                    "id": "6ec6fe9d-2b71-422a-b0d2-7ae99cc4c5d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41885c7-bb18-4f21-94ee-b2e745c67fcb",
                    "LayerId": "7fbd77b9-e8c1-4831-8482-d3effbed954b"
                }
            ]
        },
        {
            "id": "0efd55b2-96ae-4320-889c-2818dbf48e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe001bc5-3ff5-42f6-ade6-dd40eaa098c7",
            "compositeImage": {
                "id": "c48930a6-21c0-4bf6-9956-671c3636d238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0efd55b2-96ae-4320-889c-2818dbf48e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc58851b-bd10-452f-829b-f341b2fea798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0efd55b2-96ae-4320-889c-2818dbf48e39",
                    "LayerId": "e896b03c-d2bd-40c3-bb24-77c0c50bde08"
                },
                {
                    "id": "f7d14f87-9cd9-44d0-bca3-c296242e8795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0efd55b2-96ae-4320-889c-2818dbf48e39",
                    "LayerId": "6105828a-1c98-454b-b515-0c3f216337e9"
                },
                {
                    "id": "4d99ba27-b298-4c97-9c91-d3050cad2c17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0efd55b2-96ae-4320-889c-2818dbf48e39",
                    "LayerId": "7fbd77b9-e8c1-4831-8482-d3effbed954b"
                }
            ]
        },
        {
            "id": "1abf0fd4-64e5-4eea-9043-8c180b65abb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe001bc5-3ff5-42f6-ade6-dd40eaa098c7",
            "compositeImage": {
                "id": "5f2c4cd3-9489-4948-8e96-5eb12a007718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1abf0fd4-64e5-4eea-9043-8c180b65abb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9148aa46-caa1-48ca-b7c7-bbc9d75cd5fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1abf0fd4-64e5-4eea-9043-8c180b65abb0",
                    "LayerId": "e896b03c-d2bd-40c3-bb24-77c0c50bde08"
                },
                {
                    "id": "900bc0f4-e2b1-41b7-8fde-beaf3b886e77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1abf0fd4-64e5-4eea-9043-8c180b65abb0",
                    "LayerId": "6105828a-1c98-454b-b515-0c3f216337e9"
                },
                {
                    "id": "ac10225e-4e2c-432e-8e45-9df48479671b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1abf0fd4-64e5-4eea-9043-8c180b65abb0",
                    "LayerId": "7fbd77b9-e8c1-4831-8482-d3effbed954b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e896b03c-d2bd-40c3-bb24-77c0c50bde08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe001bc5-3ff5-42f6-ade6-dd40eaa098c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6105828a-1c98-454b-b515-0c3f216337e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe001bc5-3ff5-42f6-ade6-dd40eaa098c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7fbd77b9-e8c1-4831-8482-d3effbed954b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe001bc5-3ff5-42f6-ade6-dd40eaa098c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Copied Layer 2",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}