{
    "id": "f80fffd5-5b34-45bc-a7de-a0b0bad5daf5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Bed",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "56a229ff-8f87-4d37-852a-a7e4e45044ed",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "594d8c3a-39fb-4403-bff2-caabe33ddcaf",
    "visible": true
}