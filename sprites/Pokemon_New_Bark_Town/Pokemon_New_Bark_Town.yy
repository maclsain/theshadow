{
    "id": "3122cce1-f9c7-41fa-a7b1-0321df1a2bfd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Pokemon_New_Bark_Town",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 601,
    "bbox_left": 0,
    "bbox_right": 758,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1def884-8215-429c-a8c2-7f5151ae0f35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3122cce1-f9c7-41fa-a7b1-0321df1a2bfd",
            "compositeImage": {
                "id": "d0b522d1-0c45-4775-b449-b589c370feb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1def884-8215-429c-a8c2-7f5151ae0f35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc8967e9-608b-469d-bc20-bf0181576cd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1def884-8215-429c-a8c2-7f5151ae0f35",
                    "LayerId": "bcc3bcd3-66a9-4316-8a8c-93434a43e311"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 602,
    "layers": [
        {
            "id": "bcc3bcd3-66a9-4316-8a8c-93434a43e311",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3122cce1-f9c7-41fa-a7b1-0321df1a2bfd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 759,
    "xorig": 0,
    "yorig": 0
}